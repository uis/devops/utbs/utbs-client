# ---------------------------------------------------------------------
# Master configuration file for Sphinx python documentation build.
# ---------------------------------------------------------------------

import os
import re
import sys

extensions = ['sphinx.ext.autodoc',
              'sphinxcontrib.fulltoc',
              'sphinx.ext.intersphinx']
source_encoding = 'utf-8'
master_doc = 'index'
exclude_patterns = []
templates_path = []

project = u'University Training Booking System web service API'
copyright = u'2016, Dean Rasheed'
version = '@app.version@'
release = '@app.version@'
add_module_names = False
show_authors = True

html_theme = 'classic'
html_favicon = os.path.abspath('favicon.ico')
html_static_path = []
html_domain_indices = []
html_show_sourcelink = False

sys.path.insert(0, os.path.abspath('..'))

intersphinx_mapping = {'python': ('https://docs.python.org/@python.version@',
                                  'python-@python.version@-objects.inv')}

# Sphinx doesn't process descriptions of attributes defined in __slots__, so
# extract them ourselves using a processing callback
def process_docstring(app, what, name, obj, options, lines):
    if not lines and\
       what == 'attribute' and\
       hasattr(obj, '__name__') and\
       hasattr(obj, '__objclass__') and\
       hasattr(obj.__objclass__, '__slots__') and\
       isinstance(obj.__objclass__.__slots__, dict):
        attr_docs = obj.__objclass__.__slots__.get(obj.__name__)
        if attr_docs:
            # Strip off initially blank lines
            while attr_docs.startswith('\n'): attr_docs = attr_docs[1:]

            # Reduce the indentation by the indentation of the first line
            prefix_len = 0
            while prefix_len < len(attr_docs) and\
                  attr_docs[prefix_len] == ' ': prefix_len += 1
            attr_docs = re.sub('(?m)^[ ]{%d}' % prefix_len, '', attr_docs)

            # Strip of any leading or trailing whitespace and return the
            # remaining lines
            attr_docs = attr_docs.strip()
            for line in attr_docs.split('\n'):
                lines.append(line)

# Skip certain things that are really private implementation details
def skip_member(app, what, name, obj, skip, options):
    if name in ['char_data', 'start_element', 'end_element',
                'start_child_element', 'end_child_element',
                'id', 'ref', 'unflatten', 'unflattened',
                'xml_arrays', 'xml_attrs', 'xml_elems']:
        return True
    return skip

def setup(app):
    app.connect('autodoc-process-docstring', process_docstring)
    app.connect('autodoc-skip-member', skip_member)
