#!/usr/bin/python

# --------------------------------------------------------------------------
# Copyright (c) 2013, University of Cambridge Computing Service.
#
# This file is part of the University Training Booking System client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Dean Rasheed (dev-group@ucs.cam.ac.uk)
# --------------------------------------------------------------------------

from utbsclient import *

conn = createTestConnection()
em = EventMethods(conn)

e = em.getEvent(58900, "sessions,topics")

print("")
print("Event: %s" % e.title)
print("")
print("Description")
print("===========")
print(e.description)
print("")
print("Topics covered")
print("==============")
print(e.topics[0].topic)
print("")
for s in e.sessions:
    print("Session %d:" % s.sessionNumber)
    print("  %s - %s" % (s.startTime.isoformat(), s.endTime.isoformat()))
