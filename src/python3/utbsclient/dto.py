# --------------------------------------------------------------------------
# Copyright (c) 2013, University of Cambridge Computing Service.
#
# This file is part of the University Training Booking System client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Dean Rasheed (dev-group@ucs.cam.ac.uk)
# --------------------------------------------------------------------------

"""
DTO classes for transferring data from the server to client in the web
service API.

All web service API methods return an instance or a list of one of these DTO
classes, or a primitive type such as a bool, int or string.

In the case of an error, a :any:`UTBSException` will be raised which will
contain an instance of a :any:`UTBSError` DTO.
"""

import base64
from datetime import datetime
from xml.parsers import expat

import sys
if sys.hexversion < 0x02040000:
    from sets import Set as set

_MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

def parse_datetime(date_str):
    """ Parse a datetime string from XML (ISO 8601 format). """
    from .connection import TimeZone

    s = date_str.strip()

    # The input is expected to be something like one of the following:
    #  - "2016-10-28T14:15:00+01:00"
    #  - "2016-10-31T09:15:00Z"           (equivalent to +00:00 timezone)
    #  - "2016-09-23T17:46:42.479+01:00"  (fractional seconds)

    # Parse the first 6 fields, which are in a fixed format
    yr = int(s[:4])
    mo = int(s[5:7])
    dt = int(s[8:10])
    hr = int(s[11:13])
    mn = int(s[14:16])
    sc = int(s[17:19])
    s = s[19:]

    # Parse any fractional seconds
    us = 0
    if s[0] == '.':
        s = s[1:]
        factor = 1000000.0
        while s[0].isdigit():
            us = us*10 + int(s[0])
            factor = factor/10
            s = s[1:]
        us = int(us*factor)

    # Parse the timezone
    if s[0] == 'Z':
        offset = 0
    else:
        if s[0] == '+': sign = 1
        elif s[0] == '-': sign = -1
        else: raise 'Invalid timezone %s' % s
        offset = sign * (int(s[1:3]) * 60 + int(s[4:6]))

    return datetime(yr, mo, dt, hr, mn, sc, us, TimeZone(minutes=offset))

class UTBSDto(object):
    """
    Base class for all DTO classes.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    # All properties
    __slots__ = []

    # Properties marked as @XmlAttribute in the JAXB class
    xml_attrs = set()

    # Properties marked as @XmlElement in the JAXB class
    xml_elems = set()

    # Properties marked as @XmlElementWrapper in the JAXB class
    xml_arrays = set()

    def __init__(self, attrs={}):
        """
        Create a UTBSDto from the attributes of an XML node. This just sets
        the properties marked as @XmlAttribute in the JAXB class.
        """
        for attr in self.__class__.__slots__:
            setattr(self, attr, None)
        for attr in self.__class__.xml_attrs:
            setattr(self, attr, attrs.get(attr))

    def start_child_element(self, tagname):
        """
        Start element callback invoked during XML parsing when the opening
        tag of a child element is encountered. This creates and returns any
        properties marked as @XmlElementWrapper in the JAXB class, so that
        child collections can be populated.
        """
        if tagname in self.__class__.xml_arrays:
            if getattr(self, tagname) == None: setattr(self, tagname, [])
            return getattr(self, tagname)
        return None

    def end_child_element(self, tagname, data):
        """
        End element callback invoked during XML parsing when the end tag of
        a child element is encountered, and the tag's data is available. This
        sets the value of any properties marked as @XmlElement in the JAXB
        class.
        """
        if tagname in self.__class__.xml_elems:
            setattr(self, tagname, data)

# --------------------------------------------------------------------------
# UTBSProvider: see uk.ac.cam.ucs.utbs.dto.UTBSProvider.java
# --------------------------------------------------------------------------
class UTBSProvider(UTBSDto):
    """
    Class representing a training provider returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "shortName":
            """
            str
              The provider's unique short name.
            """,
        "name":
            """
            str
              The provider's full name (as it appears in the provider
              selection menu of the UTBS).
            """,
        "title":
            """
            str
              The provider's title (as it appears in the title bar of the
              UTBS).
            """,
        "institution":
            """
            :any:`UTBSInstitution`
              The institution that owns the provider. Only the
              :any:`instid <UTBSInstitution.instid>` field will be populated
              unless the `fetch` parameter included the ``"institution"``
              option.
            """,
        "urlPrefix":
            """
            str
              The unique URL prefix used for the provider's pages in the
              UTBS.
            """,
        "contactEmail":
            """
            str
              The provider's contact email address.
            """,
        "phoneNumber":
            """
            str
              The provider's contact phone number.
            """,
        "homePage":
            """
            str
              The Wiki text for the provider's main home page.
            """,
        "venueInfo":
            """
            str
              The Wiki text describing the venues belonging to the provider.
            """,
        "currentProgramme":
            """
            :any:`UTBSProgramme`
              The provider's current programme. This will only be populated
              if the `fetch` parameter included the ``"current_programme"``
              option.
            """,
        "programmes":
            """
            list of :any:`UTBSProgramme`
              A list of all the provider's programmes. This will only be
              populated if the `fetch` parameter included the
              ``"programmes"`` option.
            """,
        "themes":
            """
            list of :any:`UTBSTheme`
              A list of all the themes belonging to the provider. This will
              only be populated if the `fetch` parameter included the
              ``"themes"`` option.
            """,
        "venues":
            """
            list of :any:`UTBSVenue`
              A list of all the venues belonging to the provider. This will
              only be populated if the `fetch` parameter included the
              ``"venues"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this provider within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a provider element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["id", "ref"])

    xml_elems = set(["shortName", "name", "title", "institution",
                     "urlPrefix", "contactEmail", "phoneNumber",
                     "homePage", "venueInfo", "currentProgramme"])

    xml_arrays = set(["programmes", "themes", "venues"])

    def __init__(self, attrs={}):
        """ Create a UTBSProvider from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        self.unflattened = False

    def unflatten(self, em):
        """ Unflatten a single UTBSProvider. """
        if self.ref:
            provider = em.get_provider(self.ref)
            if not provider.unflattened:
                provider.unflattened = True
                if provider.institution:
                    provider.institution = provider.institution.unflatten(em)
                if provider.currentProgramme:
                    provider.currentProgramme =\
                        provider.currentProgramme.unflatten(em)
                unflatten_programmes(em, provider.programmes)
                unflatten_themes(em, provider.themes)
                unflatten_venues(em, provider.venues)
            return provider
        return self

def unflatten_providers(em, providers):
    """ Unflatten a list of UTBSProvider objects (done in place). """
    if providers:
        for idx, provider in enumerate(providers):
            providers[idx] = provider.unflatten(em)

# --------------------------------------------------------------------------
# UTBSProgramme: see uk.ac.cam.ucs.utbs.dto.UTBSProgramme.java
# --------------------------------------------------------------------------
class UTBSProgramme(UTBSDto):
    """
    Class representing a programme of events returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "programmeId":
            """
            int
              The unique identifier of the programme.
            """,
        "provider":
            """
            :any:`UTBSProvider`
              The programme's provider. This will only be populated if the
              `fetch` parameter included the ``"provider"`` option.
            """,
        "shortName":
            """
            str
              The programme's short name (as it appears in some reports).
            """,
        "title":
            """
            str
              The programme's title (as it appears in most of the UTBS).
            """,
        "status":
            """
            str
              The programme's status (``"published"`` or ``"completed"``).
            """,
        "startDate":
            """
            datetime
              The programme's start date.
            """,
        "endDate":
            """
            datetime
              The programme's end date.
            """,
        "events":
            """
            list of :any:`UTBSEvent`
              A list of the programme's events. This will only be populated
              if the `fetch` parameter included the ``"events"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this programme within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a programme element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["programmeId", "id", "ref"])

    xml_elems = set(["provider", "shortName", "title", "status",
                     "startDate", "endDate"])

    xml_arrays = set(["events"])

    def __init__(self, attrs={}):
        """ Create a UTBSProgramme from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.programmeId != None:
            self.programmeId = int(self.programmeId)
        self.unflattened = False

    def end_child_element(self, tagname, data):
        """ Overridden end element callback for XML parsing. """
        UTBSDto.end_child_element(self, tagname, data)
        if tagname == "startDate" and self.startDate != None:
            self.startDate = parse_datetime(self.startDate)
        if tagname == "endDate" and self.endDate != None:
            self.endDate = parse_datetime(self.endDate)

    def unflatten(self, em):
        """ Unflatten a single UTBSProgramme. """
        if self.ref:
            programme = em.get_programme(self.ref)
            if not programme.unflattened:
                programme.unflattened = True
                if programme.provider:
                    programme.provider = programme.provider.unflatten(em)
                unflatten_events(em, programme.events)
            return programme
        return self

def unflatten_programmes(em, programmes):
    """ Unflatten a list of UTBSProgramme objects (done in place). """
    if programmes:
        for idx, programme in enumerate(programmes):
            programmes[idx] = programme.unflatten(em)

# --------------------------------------------------------------------------
# UTBSEvent: see uk.ac.cam.ucs.utbs.dto.UTBSEvent.java
# --------------------------------------------------------------------------
class UTBSEvent(UTBSDto):
    """
    Class representing an event returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "eventId":
            """
            int
              The unique identifier of the event.
            """,
        "provider":
            """
            :any:`UTBSProvider`
              The event's provider. This will only be populated if the
              `fetch` parameter included the ``"provider"`` option.
            """,
        "programme":
            """
            :any:`UTBSProgramme`
              The programme to which the event belongs. This will only be
              populated if the `fetch` parameter included the ``"programme"``
              option.
            """,
        "course":
            """
            :any:`UTBSCourse`
              The course that the event is for.
            """,
        "startDate":
            """
            datetime
              The start date and time of the event.
            """,
        "endDate":
            """
            datetime
              The end date and time of the event.
            """,
        "title":
            """
            str
              The event's title.
            """,
        "courseType":
            """
            str
              The event's course type. This will be one of
              ``"instructor-led"``, ``"self-taught"`` or
              ``"facility-booking"``.
            """,
        "subType":
            """
            str
              The sub-type of the event. The possible values vary, depending
              on the event's course type.
            """,
        "status":
            """
            str
              The event's status (``"published"``, ``"completed"``,
              ``"postponed"`` or ``"cancelled"``).
            """,
        "targetAudience":
            """
            str
              The event's target audience.
            """,
        "description":
            """
            str
              The event's description.
            """,
        "duration":
            """
            str
              textual description of the event's duration.
            """,
        "frequency":
            """
            str
              How frequently the event is typically run.
            """,
        "format":
            """
            str
              The format of the event (how it is taught).
            """,
        "prerequisites":
            """
            str
              Any prerequisites for the event.
            """,
        "aims":
            """
            str
              The aims of the course.
            """,
        "objectives":
            """
            str
              The objectives of the course.
            """,
        "systemRequirements":
            """
            str
              For self-taught courses, any system requirements for
              participants.
            """,
        "notes":
            """
            str
              Any additional notes about the course.
            """,
        "newEvent":
            """
            bool
              A flag indicating whether this is a new course.
            """,
        "updatedEvent":
            """
            bool
              A flag indicating whether this course has been recently
              updated.
            """,
        "specialEvent":
            """
            bool
              A flag indicating whether this is a special event, such as a
              one-off course that won't be repeated.
            """,
        "extraRun":
            """
            bool
              A flag indicating whether this is an extra run event added to
              meet unexpected demand.
            """,
        "beginnersEvent":
            """
            bool
              A flag indicating whether this is an event for beginners, with
              no significant prerequisites.
            """,
        "hasPrerequisites":
            """
            bool
              A flag indicating whether this course assumes significant
              prerequisite experience from participants.
            """,
        "showOnTimetable":
            """
            bool
              A flag indicating whether or not this event is shown on the
              timetable.
            """,
        "showOnThemesPage":
            """
            bool
              A flag indicating whether or not this event is shown on the
              themes page.
            """,
        "maxParticipants":
            """
            int
              The maximum number of participants.
            """,
        "topics":
            """
            list of :any:`UTBSEventTopic`
              A list of the event's topics. This will only be populated if
              the `fetch` parameter included the ``"topics"`` option.
            """,
        "extraInfo":
            """
            list of :any:`UTBSEventExtraInfo`
              A list of any extra information sections giving further details
              about the event. This will only be populated in the `fetch`
              parameter included the ``"extra_info"`` option.
            """,
        "relatedCourses":
            """
            list of :any:`UTBSCourse`
              A list of the courses related to the event. This will only be
              populated if the `fetch` parameter included the
              ``"related_courses"`` option.
            """,
        "themes":
            """
            list of :any:`UTBSTheme`
              A list of the event's themes. This will only be populated if
              the `fetch` parameter included the ``"themes"`` option.
            """,
        "sessions":
            """
            list of :any:`UTBSEventSession`
              A list of the event's sessions. This will only be populated if
              the `fetch` parameter included the ``"sessions"`` option.
            """,
        "venues":
            """
            list of :any:`UTBSVenue`
               list of the venues that the event is held in (typically just
               one). This will only be populated if the `fetch` parameter
               included the ``"venues"`` option.
            """,
        "trainers":
            """
            list of :any:`UTBSSessionTrainer`
              A list of the event's trainers. This will only be populated if
              the `fetch` parameter included the ``"trainers"`` option.
            """,
        "bookings":
            """
            list of :any:`UTBSEventBooking`
              A list of the event's bookings. This will only be populated if
              the `fetch` parameter included the ``"bookings"`` option, and
              is only available to authenticated users with appropriate
              privileges for the event. This may be an incomplete list, if
              you do not have permission to view all the event's bookings.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this event within the returned
              XML/JSON document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "ref":
            """
            str
              A reference (by id) to an event element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["eventId", "newEvent", "updatedEvent", "specialEvent",
                     "extraRun", "beginnersEvent", "hasPrerequisites",
                     "showOnTimetable", "showOnThemesPage", "id", "ref"])

    xml_elems = set(["provider", "programme", "course",
                     "startDate", "endDate", "title",
                     "courseType", "subType", "status",
                     "targetAudience", "description",
                     "duration", "frequency", "format",
                     "prerequisites", "aims", "objectives",
                     "systemRequirements", "notes",
                     "maxParticipants"])

    xml_arrays = set(["topics", "extraInfo", "relatedCourses",
                      "themes", "sessions", "venues", "trainers",
                      "bookings"])

    def __init__(self, attrs={}):
        """ Create a UTBSEvent from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.eventId != None:
            self.eventId = int(self.eventId)
        if self.newEvent != None:
            self.newEvent = self.newEvent.lower() == "true"
        if self.updatedEvent != None:
            self.updatedEvent = self.updatedEvent.lower() == "true"
        if self.specialEvent != None:
            self.specialEvent = self.specialEvent.lower() == "true"
        if self.extraRun != None:
            self.extraRun = self.extraRun.lower() == "true"
        if self.beginnersEvent != None:
            self.beginnersEvent = self.beginnersEvent.lower() == "true"
        if self.hasPrerequisites != None:
            self.hasPrerequisites = self.hasPrerequisites.lower() == "true"
        if self.showOnTimetable != None:
            self.showOnTimetable = self.showOnTimetable.lower() == "true"
        if self.showOnThemesPage != None:
            self.showOnThemesPage = self.showOnThemesPage.lower() == "true"
        self.unflattened = False

    def end_child_element(self, tagname, data):
        """ Overridden end element callback for XML parsing. """
        UTBSDto.end_child_element(self, tagname, data)
        if tagname == "startDate" and self.startDate != None:
            self.startDate = parse_datetime(self.startDate)
        if tagname == "endDate" and self.endDate != None:
            self.endDate = parse_datetime(self.endDate)
        if tagname == "maxParticipants" and self.maxParticipants != None:
            self.maxParticipants = int(self.maxParticipants)

    def unflatten(self, em):
        """ Unflatten a single UTBSEvent. """
        if self.ref:
            event = em.get_event(self.ref)
            if not event.unflattened:
                event.unflattened = True
                if event.provider:
                    event.provider = event.provider.unflatten(em)
                if event.programme:
                    event.programme = event.programme.unflatten(em)
                if event.course:
                    event.course = event.course.unflatten(em)
                unflatten_courses(em, event.relatedCourses)
                unflatten_themes(em, event.themes)
                unflatten_sessions(em, event.sessions)
                unflatten_venues(em, event.venues)
                unflatten_bookings(em, event.bookings)
            return event
        return self

def unflatten_events(em, events):
    """ Unflatten a list of UTBSEvent objects (done in place). """
    if events:
        for idx, event in enumerate(events):
            events[idx] = event.unflatten(em)

# --------------------------------------------------------------------------
# UTBSCourse: see uk.ac.cam.ucs.utbs.dto.UTBSCourse.java
# --------------------------------------------------------------------------
class UTBSCourse(UTBSDto):
    """
    Class representing a course returned by the web service API.

    Note that all the course details are actually on the individual events
    for the course, and may vary each time the course is run.

    In rare cases, a course may switch provider, and so there is no provider
    field on this class, and there is no guarantee that all the course's
    events have the same provider.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "courseId":
            """
            str
              The unique identifier of the course.
            """,
        "events":
            """
            list of :any:`UTBSEvent`
              A list of the course's events. This will only be populated if
              the `fetch` parameter included the ``"events"`` option.
            """,
        "themes":
            """
            list of :any:`UTBSTheme`
              A list of the course's themes. This will only be populated if
              the `fetch` parameter included the ``"themes"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this course within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a course element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["courseId", "id", "ref"])

    xml_arrays = set(["events", "themes"])

    def __init__(self, attrs={}):
        """ Create a UTBSCourse from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        self.unflattened = False

    def unflatten(self, em):
        """ Unflatten a single UTBSCourse. """
        if self.ref:
            course = em.get_course(self.ref)
            if not course.unflattened:
                course.unflattened = True
                unflatten_events(em, course.events)
                unflatten_themes(em, course.themes)
            return course
        return self

def unflatten_courses(em, courses):
    """ Unflatten a list of UTBSCourse objects (done in place). """
    if courses:
        for idx, course in enumerate(courses):
            courses[idx] = course.unflatten(em)

# --------------------------------------------------------------------------
# UTBSEventTopic: see uk.ac.cam.ucs.utbs.dto.UTBSEventTopic.java
# --------------------------------------------------------------------------
class UTBSEventTopic(UTBSDto):
    """
    Class representing an individual topic from an event's course description
    returned by the web service API.

    The topic may refer to the event as a whole, or be associated with a
    specific session of the event, if the
    :any:`sessionNumber <UTBSEventTopic.sessionNumber>` field is set.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "sessionNumber":
            """
            int
              The number of session that the topic is associated with. This
              may be :any:`None` for topics describing the event as a whole.
            """,
        "topic":
            """
            str
              The topic's text.
            """
    }

    xml_attrs = set(["sessionNumber"])

    def __init__(self, attrs={}):
        """ Create a UTBSEventTopic from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.sessionNumber != None:
            self.sessionNumber = int(self.sessionNumber)

# --------------------------------------------------------------------------
# UTBSEventExtraInfo: see uk.ac.cam.ucs.utbs.dto.UTBSEventExtraInfo.java
# --------------------------------------------------------------------------
class UTBSEventExtraInfo(UTBSDto):
    """
    Class representing an extra information section forming part of an
    event's course description returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "heading":
            """
            str
              The section heading.
            """,
        "content":
            """
            str
              The section's text.
            """
    }

    xml_elems = set(["heading", "content"])

# --------------------------------------------------------------------------
# UTBSEventSession: see uk.ac.cam.ucs.utbs.dto.UTBSEventSession.java
# --------------------------------------------------------------------------
class UTBSEventSession(UTBSDto):
    """
    Class representing an individual event session returned by the web
    service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "sessionNumber":
            """
            int
              The session number.
            """,
        "startTime":
            """
            datetime
              The session's start date and time.
            """,
        "endTime":
            """
            datetime
              The session's end date and time.
            """,
        "venue":
            """
            :any:`UTBSVenue`
              The session's venue. This will only be populated if the `fetch`
              parameter included the ``"venue"`` option.
            """,
        "optional":
            """
            bool
              Whether or not this is an optional session.
            """,
        "trainers":
            """
            list of :any:`UTBSSessionTrainer`
              A list of the session's trainers. This will only be populated
              if the `fetch` parameter included the ``"trainers"`` option.
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["sessionNumber", "optional"])

    xml_elems = set(["startTime", "endTime", "venue"])

    xml_arrays = set(["trainers"])

    def __init__(self, attrs={}):
        """ Create a UTBSEventSession from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.sessionNumber != None:
            self.sessionNumber = int(self.sessionNumber)
        if self.optional != None:
            self.optional = self.optional.lower() == "true"
        self.unflattened = False

    def end_child_element(self, tagname, data):
        """ Overridden end element callback for XML parsing. """
        UTBSDto.end_child_element(self, tagname, data)
        if tagname == "startTime" and self.startTime != None:
            self.startTime = parse_datetime(self.startTime)
        if tagname == "endTime" and self.endTime != None:
            self.endTime = parse_datetime(self.endTime)

    def unflatten(self, em):
        """ Unflatten a single UTBSEventSession. """
        if not self.unflattened:
            self.unflattened = True
            if self.venue:
                self.venue = self.venue.unflatten(em)
        return self

def unflatten_sessions(em, sessions):
    """ Unflatten a list of UTBSEventSession objects (done in place). """
    if sessions:
        for idx, session in enumerate(sessions):
            sessions[idx] = session.unflatten(em)

# --------------------------------------------------------------------------
# UTBSSessionTrainer: see uk.ac.cam.ucs.utbs.dto.UTBSSessionTrainer.java
# --------------------------------------------------------------------------
class UTBSSessionTrainer(UTBSDto):
    """
    Class representing a trainer of an event session returned by the web
    service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "crsid":
            """
            str
              The trainer's CRSid.
            """,
        "name":
            """
            str
              The trainer's name.
            """
    }

    xml_attrs = set(["crsid"])

# --------------------------------------------------------------------------
# UTBSTheme: see uk.ac.cam.ucs.utbs.dto.UTBSTheme.java
# --------------------------------------------------------------------------
class UTBSTheme(UTBSDto):
    """
    Class representing a theme returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "themeId":
            """
            int
              The unique internal identifier of the theme.
            """,
        "provider":
            """
            :any:`UTBSProvider`
              The theme's provider. This will only be populated if the
              `fetch` parameter included the ``"provider"`` option.
            """,
        "shortName":
            """
            str
              The theme's short name (as it appears in some URLs).
            """,
        "title":
            """
            str
              The theme's title (as it appears in most of the UTBS).
            """,
        "courses":
            """
            list of :any:`UTBSCourse`
              A list of the theme's courses. This will only be populated if
              the `fetch` parameter included the ``"courses"`` option.
            """,
        "events":
            """
            list of :any:`UTBSEvent`
              A list of the theme's events. This will only be populated if
              the `fetch` parameter included the ``"events"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this theme within the returned
              XML/JSON document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a theme element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["themeId", "id", "ref"])

    xml_elems = set(["provider", "shortName", "title"])

    xml_arrays = set(["courses", "events"])

    def __init__(self, attrs={}):
        """ Create a UTBSTheme from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.themeId != None:
            self.themeId = int(self.themeId)
        self.unflattened = False

    def unflatten(self, em):
        """ Unflatten a single UTBSTheme. """
        if self.ref:
            theme = em.get_theme(self.ref)
            if not theme.unflattened:
                theme.unflattened = True
                if theme.provider:
                    theme.provider = theme.provider.unflatten(em)
                unflatten_courses(em, theme.courses)
                unflatten_events(em, theme.events)
            return theme
        return self

def unflatten_themes(em, themes):
    """ Unflatten a list of UTBSTheme objects (done in place). """
    if themes:
        for idx, theme in enumerate(themes):
            themes[idx] = theme.unflatten(em)

# --------------------------------------------------------------------------
# UTBSVenue: see uk.ac.cam.ucs.utbs.dto.UTBSVenue.java
# --------------------------------------------------------------------------
class UTBSVenue(UTBSDto):
    """
    Class representing a venue returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "venueId":
            """
            int
              The unique identifier of the venue.
            """,
        "shortName":
            """
            str
              The venue's unique short name (as it appears on the timetable).
            """,
        "provider":
            """
            :any:`UTBSProvider`
              The venue's provider. This will only be populated if the
              `fetch` parameter included the ``"provider"`` option.
            """,
        "name":
            """
            str
              The venue's full name.
            """,
        "description":
            """
            str
              The venue's description.
            """,
        "capacity":
            """
            int
              The number of seats or workstations in the venue.
            """,
        "location":
            """
            str
              The venue's location (address).
            """,
        "directions":
            """
            str
              Directions to get to the venue.
            """,
        "disabledAccess":
            """
            str
              Information about access and emergency escape for those with
              disabilities.
            """,
        "parkingAdvice":
            """
            str
              Availablity of parking near the venue.
            """,
        "mapUrl":
            """
            str
              The URL of a map showing the venue's location.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this venue within the returned
              XML/JSON document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a venue element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["venueId", "id", "ref"])

    xml_elems = set(["shortName", "provider", "name", "description",
                     "capacity", "location", "directions", "disabledAccess",
                     "parkingAdvice", "mapUrl"])

    def __init__(self, attrs={}):
        """ Create a UTBSVenue from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.venueId != None:
            self.venueId = int(self.venueId)
        self.unflattened = False

    def end_child_element(self, tagname, data):
        """ Overridden end element callback for XML parsing. """
        UTBSDto.end_child_element(self, tagname, data)
        if tagname == "capacity" and self.capacity != None:
            self.capacity = int(self.capacity)

    def unflatten(self, em):
        """ Unflatten a single UTBSVenue. """
        if self.ref:
            venue = em.get_venue(self.ref)
            if not venue.unflattened:
                venue.unflattened = True
                if venue.provider:
                    venue.provider = venue.provider.unflatten(em)
            return venue
        return self

def unflatten_venues(em, venues):
    """ Unflatten a list of UTBSVenue objects (done in place). """
    if venues:
        for idx, venue in enumerate(venues):
            venues[idx] = venue.unflatten(em)

# --------------------------------------------------------------------------
# UTBSPerson: see uk.ac.cam.ucs.utbs.dto.UTBSPerson.java
# --------------------------------------------------------------------------
class UTBSPerson(UTBSDto):
    """
    Class representing a person returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "crsid":
            """
            str
              The unique identifier of the person.
            """,
        "name":
            """
            str
              The person's name.
            """,
        "email":
            """
            str
              The person's email address.
            """,
        "phoneNumber":
            """
            str
              The person's phone number.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this person within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a person element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """
    }

    xml_attrs = set(["crsid", "id", "ref"])

    xml_elems = set(["name", "email", "phoneNumber"])

    def unflatten(self, em):
        """ Unflatten a single UTBSPerson. """
        if self.ref:
            return em.get_person(self.ref)
        return self

def unflatten_people(em, people):
    """ Unflatten a list of UTBSPerson objects (done in place). """
    if people:
        for idx, person in enumerate(people):
            people[idx] = person.unflatten(em)

# --------------------------------------------------------------------------
# UTBSEventBooking: see uk.ac.cam.ucs.utbs.dto.UTBSEventBooking.java
# --------------------------------------------------------------------------
class UTBSEventBooking(UTBSDto):
    """
    Class representing a booking on an event returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "bookingId":
            """
            int
              The unique identifier of the booking.
            """,
        "event":
            """
            :any:`UTBSEvent`
              The event booked. Only the event ID field will be populated
              unless the `fetch` parameter included the ``"event"`` option.
            """,
        "participant":
            """
            :any:`UTBSPerson`
              The person the booking is for. Only the CRSid field will be
              populated unless the `fetch` parameter included the
              ``"participant"`` option.
            """,
        "bookedBy":
            """
            :any:`UTBSPerson`
              The person who made the booking. Only the CRSid field will be
              populated unless the `fetch` parameter included the
              ``"booked_by"`` option.
            """,
        "bookingTime":
            """
            datetime
              The time that the booking was originally made.
            """,
        "status":
            """
            str
              The status of the booking (``"provisional"``, ``"booked"``,
              ``"standby"``, ``"offered"``, ``"no response"``, ``"waiting"``,
              ``"interested"``, ``"cancelled"``, ``"completed"`` or
              ``"did not book"``). Confirmed bookings will have a status of
              ``"booked"``.

              For events with optional sessions, the set of sessions booked
              is recorded in
              :any:`sessionBookings <UTBSEventBooking.sessionBookings>`.
            """,
        "institution":
            """
            :any:`UTBSInstitution`
              The participant's institution chosen at the time the booking
              was made. This institution may no longer be the participant's
              institution, and it may now no longer exist (i.e., it may be
              cancelled). Only the :any:`instid <UTBSInstitution.instid>`
              field will be populated unless the `fetch` parameter included
              the ``"institution"`` option.
            """,
        "misStatus":
            """
            str
              The staff/student status of the participant at the time the
              booking was made. For members of the University of Cambridge,
              this will be one of the following: ``"staff"``, ``"student"``,
              ``"staff,student"`` or ``""``. For external participants, it
              will be :any:`None`.
            """,
        "specialRequirements":
            """
            str
              Any special requirements the participant has.
            """,
        "attended":
            """
            bool
              Whether or not the participant attended all the non-optional
              sessions of the event. This will be :any:`None` until
              attendance has been recorded for all sessions. Attendance on
              individual sessions is recorded in
              :any:`sessionBookings <UTBSEventBooking.sessionBookings>`.
            """,
        "passed":
            """
            bool
              Whether or not the participant passed all the non-optional
              sessions of an assessed event. This will be :any:`None` for
              events that are not assessed. Whether or not individual
              sessions were passed is recorded in
              :any:`sessionBookings <UTBSEventBooking.sessionBookings>`.
            """,
        "satisfied":
            """
            bool
              Whether or not the participant was satisfied with the event.
              This will be :any:`None` if user feedback was not recorded.
            """,
        "sessionBookings":
            """
            list of :any:`UTBSSessionBooking`
              A list of the session booking details. This records the
              individual sessions that were booked, as well as sessions
              attended and (for assessed events) sessions passed. This will
              only be populated if the `fetch` parameter included the
              ``"session_bookings"`` option.
            """,
        "institutions":
            """
            list of :any:`UTBSInstitution`
              A list of all the participant's institutions, as they were at
              the time the booking was made. This may include now-cancelled
              institutions. This will only be populated if the `fetch`
              parameter included the ``"institutions"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this booking within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to a booking element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["bookingId", "attended", "passed",
                     "satisfied", "id", "ref"])

    xml_elems = set(["event", "participant", "bookedBy", "bookingTime",
                     "status", "institution", "misStatus",
                     "specialRequirements"])

    xml_arrays = set(["sessionBookings", "institutions"])

    def __init__(self, attrs={}):
        """ Create a UTBSEventBooking from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.bookingId != None:
            self.bookingId = int(self.bookingId)
        if self.attended != None:
            self.attended = self.attended.lower() == "true"
        if self.passed != None:
            self.passed = self.passed.lower() == "true"
        if self.satisfied != None:
            self.satisfied = self.satisfied.lower() == "true"
        self.unflattened = False

    def end_child_element(self, tagname, data):
        """ Overridden end element callback for XML parsing. """
        UTBSDto.end_child_element(self, tagname, data)
        if tagname == "bookingTime" and self.bookingTime != None:
            self.bookingTime = parse_datetime(self.bookingTime)

    def unflatten(self, em):
        """ Unflatten a single UTBSEventBooking. """
        if self.ref:
            booking = em.get_booking(self.ref)
            if not booking.unflattened:
                booking.unflattened = True
                if booking.event:
                    booking.event = booking.event.unflatten(em)
                if booking.participant:
                    booking.participant = booking.participant.unflatten(em)
                if booking.bookedBy:
                    booking.bookedBy = booking.bookedBy.unflatten(em)
                if booking.institution:
                    booking.institution = booking.institution.unflatten(em)
                unflatten_institutions(em, booking.institutions)
            return booking
        return self

def unflatten_bookings(em, bookings):
    """ Unflatten a list of UTBSEventBooking objects (done in place). """
    if bookings:
        for idx, booking in enumerate(bookings):
            bookings[idx] = booking.unflatten(em)

# --------------------------------------------------------------------------
# UTBSSessionBooking: see uk.ac.cam.ucs.utbs.dto.UTBSSessionBooking.java
# --------------------------------------------------------------------------
class UTBSSessionBooking(UTBSDto):
    """
    Class representing the state of a booking as related to a single session
    of an event returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "sessionNumber":
            """
            int
              The session number.
            """,
        "booked":
            """
            bool
              Whether or not the session was booked.
            """,
        "attended":
            """
            bool
              Whether or not the participant attended the session.
            """,
        "passed":
            """
            bool
              Whether or not the participant passed the session (for assessed
              events).
            """
    }

    xml_attrs = set(["sessionNumber", "booked", "attended", "passed"])

    def __init__(self, attrs={}):
        """ Create a UTBSSessionBooking from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.sessionNumber != None:
            self.sessionNumber = int(self.sessionNumber)
        if self.booked != None:
            self.booked = self.booked.lower() == "true"
        if self.attended != None:
            self.attended = self.attended.lower() == "true"
        if self.passed != None:
            self.passed = self.passed.lower() == "true"

# --------------------------------------------------------------------------
# UTBSInstitution: see uk.ac.cam.ucs.utbs.dto.UTBSInstitution.java
# --------------------------------------------------------------------------
class UTBSInstitution(UTBSDto):
    """
    Class representing a Lookup institution returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "instid":
            """
            str
              The unique identifier of the institution.
            """,
        "shortName":
            """
            str
              The institution's short name or acronym.
            """,
        "name":
            """
            str
              The institution's full name.
            """,
        "parent":
            """
            :any:`UTBSInstitution`
              The institution's parent institution. Only the
              :any:`instid <UTBSInstitution.instid>` field will be populated
              unless the `fetch` parameter included the ``"parent"`` option.
            """,
        "school":
            """
            :any:`UTBSInstitution`
              The institution's school institution. Only the
              :any:`instid <UTBSInstitution.instid>` field will be populated
              unless the `fetch` parameter included the ``"school"`` option.
            """,
        "cufsCode":
            """
            str
              The institution's CUFS code.
            """,
        "cancelled":
            """
            bool
              A flag indicating whether this institution is cancelled (and
              hence no longer exists).
            """,
        "children":
            """
            list of :any:`UTBSInstitution`
              A list of the institution's child institutions. This will only
              include non-cancelled institutions, and will only be populated
              if the `fetch` parameter included the ``"children"`` option.
            """,
        "id":
            """
            str
              An ID that can uniquely identify this institution within the
              returned XML/JSON document. This is only used in the flattened
              XML/JSON representation (if the "flatten" parameter is
              specified).
            """,
        "ref":
            """
            str
              A reference (by id) to an institution element in the XML/JSON
              document. This is only used in the flattened XML/JSON
              representation (if the "flatten" parameter is specified).
            """,
        "unflattened":
            """
            bool
              Flag to prevent infinite recursion due to circular references.
            """
    }

    xml_attrs = set(["instid", "cancelled", "id", "ref"])

    xml_elems = set(["shortName", "name", "parent", "school", "cufsCode"])

    xml_arrays = set(["children"])

    def __init__(self, attrs={}):
        """ Create a UTBSInstitution from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.cancelled != None:
            self.cancelled = self.cancelled.lower() == "true"
        self.unflattened = False

    def unflatten(self, em):
        """ Unflatten a single UTBSInstitution. """
        if self.ref:
            institution = em.get_institution(self.ref)
            if not institution.unflattened:
                institution.unflattened = True
                if institution.parent:
                    institution.parent = institution.parent.unflatten(em)
                if institution.school:
                    institution.school = institution.school.unflatten(em)
                unflatten_institutions(em, institution.children)
            return institution
        return self

def unflatten_institutions(em, institutions):
    """ Unflatten a list of UTBSInstitution objects (done in place). """
    if institutions:
        for idx, institution in enumerate(institutions):
            institutions[idx] = institution.unflatten(em)

# --------------------------------------------------------------------------
# UTBSError: see uk.ac.cam.ucs.utbs.dto.UTBSError.java
# --------------------------------------------------------------------------
class UTBSError(UTBSDto):
    """
    Class representing an error returned by the web service API.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "status":
            """
            int
              The HTTP error status code.
            """,
        "code":
            """
            str
              A short textual description of the error status code.
            """,
        "message":
            """
            str
              A short textual description of the error message (typically one
              line).
            """,
        "details":
            """
            str
              The full details of the error (e.g., a Java stack trace).
            """
    }

    xml_attrs = set(["status"])

    xml_elems = set(["code", "message", "details"])

    def __init__(self, attrs={}):
        """ Create a UTBSError from the attributes of an XML node. """
        UTBSDto.__init__(self, attrs)
        if self.status != None:
            self.status = int(self.status)

# --------------------------------------------------------------------------
# UTBSResult: see uk.ac.cam.ucs.utbs.dto.UTBSResult.java
# --------------------------------------------------------------------------
class UTBSResult(UTBSDto):
    """
    Class representing the top-level container for all results.

    This may be just a simple textual value or it may contain more complex
    entities such as providers, programmes, events, courses, themes, venues,
    people, bookings or institutions.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    __slots__ = {
        "version":
            """
            str
              The web service API version number.
            """,
        "value":
            """
            str
              The value returned by methods that return a simple textual
              value.
            """,
        "provider":
            """
            :any:`UTBSProvider`
              The provider returned by methods that return a single provider.

              Note that methods that may return multiple providers will
              always use the :any:`providers <UTBSResult.providers>` field,
              even if only one provider was returned.
            """,
        "programme":
            """
            :any:`UTBSProgramme`
              The programme returned by methods that return a single
              programme.

              Note that methods that may return multiple programmes will
              always use the :any:`programmes <UTBSResult.programmes>`
              field, even if only one programme was returned.
            """,
        "event":
            """
            :any:`UTBSEvent`
              The event returned by methods that return a single event.

              Note that methods that may return multiple events will always
              use the :any:`events <UTBSResult.events>` field, even if only
              one event was returned.
            """,
        "course":
            """
            :any:`UTBSCourse`
              The course returned by methods that return a single course.

              Note that methods that may return multiple courses will always
              use the :any:`courses <UTBSResult.courses>` field, even if only
              one course was returned.
            """,
        "theme":
            """
            :any:`UTBSTheme`
              The theme returned by methods that return a single theme.

              Note that methods that may return multiple themes will always
              use the :any:`themes <UTBSResult.themes>` field, even if only
              one theme was returned.
            """,
        "venue":
            """
            :any:`UTBSVenue`
              The venue returned by methods that return a single venue.

              Note that methods that may return multiple venues will always
              use the :any:`venues <UTBSResult.venues>` field, even if only
              one venue was returned.
            """,
        "person":
            """
            :any:`UTBSPerson`
              The person returned by methods that return a single person.

              Note that methods that may return multiple people will always
              use the :any:`people <UTBSResult.people>` field, even if only
              one person was returned.
            """,
        "booking":
            """
            :any:`UTBSEventBooking`
              The booking returned by methods that return a single booking.

              Note that methods that may return multiple bookings will always
              use the :any:`bookings <UTBSResult.bookings>` field, even if
              only one booking was returned.
            """,
        "institution":
            """
            :any:`UTBSInstitution`
              The institution returned by methods that return a single
              institution.

              Note that methods that may return multiple institutions will
              always use the :any:`institutions <UTBSResult.institutions>`
              field, even if only one institution was returned.
            """,
        "error":
            """
            :any:`UTBSError`
              If the method failed, details of the error.
            """,
        "providers":
            """
            list of :any:`UTBSProvider`
              The list of providers returned by methods that may return
              multiple providers. This may be empty, or contain one or more
              providers.
            """,
        "programmes":
            """
            list of :any:`UTBSProgramme`
              The list of programmes returned by methods that may return
              multiple programmes. This may be empty, or contain one or more
              programmes.
            """,
        "events":
            """
            list of :any:`UTBSEvent`
              The list of events returned by methods that may return multiple
              events. This may be empty, or contain one or more events.
            """,
        "courses":
            """
            list of :any:`UTBSCourse`
              The list of courses returned by methods that may return
              multiple courses. This may be empty, or contain one or more
              courses.
            """,
        "themes":
            """
            list of :any:`UTBSTheme`
              The list of themes returned by methods that may return multiple
              themes. This may be empty, or contain one or more themes.
            """,
        "venues":
            """
            list of :any:`UTBSVenue`
              The list of venues returned by methods that may return multiple
              venues. This may be empty, or contain one or more venues.
            """,
        "people":
            """
            list of :any:`UTBSPerson`
              The list of people returned by methods that may return multiple
              people. This may be empty, or contain one or more people.
            """,
        "bookings":
            """
            list of :any:`UTBSEventBooking`
              The list of bookings returned by methods that may return
              multiple bookings. This may be empty, or contain one or more
              bookings.
            """,
        "institutions":
            """
            list of :any:`UTBSInstitution`
              The list of institutions returned by methods that may return
              multiple institutions. This may be empty, or contain one or
              more institutions.
            """,
        "entities":
            """
            :any:`UTBSResult.Entities`
              In the flattened XML/JSON representation, all the unique
              entities returned by the method.

              .. note::
                This will be :any:`None` unless the "flatten" parameter is
                :any:`True`.
            """
    }

    xml_attrs = set(["version"])

    xml_elems = set(["value", "provider", "programme", "event", "course",
                     "theme", "venue", "person", "booking", "institution",
                     "error", "entities"])

    xml_arrays = set(["providers", "programmes", "events", "courses",
                      "themes", "venues", "people", "bookings",
                      "institutions"])

    class Entities(UTBSDto):
        """
        Nested class to hold the full details of all the entities returned
        in a result. This is used only in the flattened result
        representation, where each of these entities will have a unique
        textual ID, and be referred to from the top-level objects returned
        (and by each other).

        In the hierarchical representation, this is not used, since all
        entities returned will be at the top-level, or directly contained in
        those top-level entities.

        .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
        """
        __slots__ = {
            "providers":
                """
                list of :any:`UTBSProvider`
                  A list of all the unique providers returned by the method.
                  This may include additional providers returned as a result
                  of the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "programmes":
                """
                list of :any:`UTBSProgramme`
                  A list of all the unique programmes returned by the method.
                  This may include additional institutions returned as a
                  result of the `fetch` parameter, so this list may contain
                  more entries than the corresponding field on the enclosing
                  class.
                """,
            "events":
                """
                list of :any:`UTBSEvent`
                  A list of all the unique events returned by the method.
                  This may include additional events returned as a result of
                  the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "courses":
                """
                list of :any:`UTBSCourse`
                  A list of all the unique courses returned by the method.
                  This may include additional courses returned as a result of
                  the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "themes":
                """
                list of :any:`UTBSTheme`
                  A list of all the unique themes returned by the method.
                  This may include additional themes returned as a result of
                  the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "venues":
                """
                list of :any:`UTBSVenue`
                  A list of all the unique venues returned by the method.
                  This may include additional venues returned as a result of
                  the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "people":
                """
                list of :any:`UTBSPerson`
                  A list of all the unique people returned by the method.
                  This may include additional people returned as a result of
                  the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "bookings":
                """
                list of :any:`UTBSEventBooking`
                  A list of all the unique bookings returned by the method.
                  This may include additional bookings returned as a result
                  of the `fetch` parameter, so this list may contain more
                  entries than the corresponding field on the enclosing
                  class.
                """,
            "institutions":
                """
                list of :any:`UTBSInstitution`
                  A list of all the unique institutions returned by the
                  method. This may include additional institutions returned
                  as a result of the `fetch` parameter, so this list may
                  contain more entries than the corresponding field on the
                  enclosing class.
                """,
        }

        xml_arrays = set(["providers", "programmes", "events", "courses",
                          "themes", "venues", "people", "bookings",
                          "institutions"])

    class EntityMap:
        """
        Nested class to assist during the unflattening process, maintaining
        efficient maps from IDs to entities (providers, programmes, events,
        courses, themes, venues, people, bookings and institutions).

        .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
        """
        def __init__(self, result):
            """
            Construct an entity map from a flattened UTBSResult.
            """
            self.providers_by_id = {}
            self.programmes_by_id = {}
            self.events_by_id = {}
            self.courses_by_id = {}
            self.themes_by_id = {}
            self.venues_by_id = {}
            self.people_by_id = {}
            self.bookings_by_id = {}
            self.institutions_by_id = {}

            if result.entities.providers:
                for provider in result.entities.providers:
                    self.providers_by_id[provider.id] = provider
            if result.entities.programmes:
                for programme in result.entities.programmes:
                    self.programmes_by_id[programme.id] = programme
            if result.entities.events:
                for event in result.entities.events:
                    self.events_by_id[event.id] = event
            if result.entities.courses:
                for course in result.entities.courses:
                    self.courses_by_id[course.id] = course
            if result.entities.themes:
                for theme in result.entities.themes:
                    self.themes_by_id[theme.id] = theme
            if result.entities.venues:
                for venue in result.entities.venues:
                    self.venues_by_id[venue.id] = venue
            if result.entities.people:
                for person in result.entities.people:
                    self.people_by_id[person.id] = person
            if result.entities.bookings:
                for booking in result.entities.bookings:
                    self.bookings_by_id[booking.id] = booking
            if result.entities.institutions:
                for institution in result.entities.institutions:
                    self.institutions_by_id[institution.id] = institution

        def get_provider(self, id):
            """
            Get a provider from the entity map, given their ID.
            """
            return self.providers_by_id.get(id)

        def get_programme(self, id):
            """
            Get a programme from the entity map, given its ID.
            """
            return self.programmes_by_id.get(id)

        def get_event(self, id):
            """
            Get an event from the entity map, given its ID.
            """
            return self.events_by_id.get(id)

        def get_course(self, id):
            """
            Get a course from the entity map, given its ID.
            """
            return self.courses_by_id.get(id)

        def get_theme(self, id):
            """
            Get a theme from the entity map, given its ID.
            """
            return self.themes_by_id.get(id)

        def get_venue(self, id):
            """
            Get a venue from the entity map, given its ID.
            """
            return self.venues_by_id.get(id)

        def get_person(self, id):
            """
            Get a person from the entity map, given its ID.
            """
            return self.people_by_id.get(id)

        def get_booking(self, id):
            """
            Get a booking from the entity map, given its ID.
            """
            return self.bookings_by_id.get(id)

        def get_institution(self, id):
            """
            Get an institution from the entity map, given its ID.
            """
            return self.institutions_by_id.get(id)

    def unflatten(self):
        """
        Unflatten this UTBSResult object, resolving any internal ID refs
        to build a fully fledged object tree.

        This is necessary if the UTBSResult was constructed from XML/JSON in
        its flattened representation (with the "flatten" parameter set to
        :any:`True`).

        On entry, the UTBSResult object may have providers, programmes,
        events, courses, themes, venues, people, bookings or institutions in
        it with "ref" fields referring to objects held in the "entities"
        lists. After unflattening, all such references will have been
        replaced by actual object references, giving an object tree that can
        be traversed normally.

        Returns this UTBSResult object, with its internals unflattened.
        """
        if self.entities:
            em = UTBSResult.EntityMap(self)

            if self.provider:
                self.provider = self.provider.unflatten(em)
            if self.programme:
                self.programme = self.programme.unflatten(em)
            if self.event:
                self.event = self.event.unflatten(em)
            if self.course:
                self.course = self.course.unflatten(em)
            if self.theme:
                self.theme = self.theme.unflatten(em)
            if self.venue:
                self.venue = self.venue.unflatten(em)
            if self.person:
                self.person = self.person.unflatten(em)
            if self.booking:
                self.booking = self.booking.unflatten(em)
            if self.institution:
                self.institution = self.institution.unflatten(em)

            unflatten_providers(em, self.providers)
            unflatten_programmes(em, self.programmes)
            unflatten_events(em, self.events)
            unflatten_courses(em, self.courses)
            unflatten_themes(em, self.themes)
            unflatten_venues(em, self.venues)
            unflatten_people(em, self.people)
            unflatten_bookings(em, self.bookings)
            unflatten_institutions(em, self.institutions)

        return self

# --------------------------------------------------------------------------
# UTBSResultParser: unmarshaller for UTBSResult objects
# --------------------------------------------------------------------------
class UTBSResultParser:
    """
    Class to parse the XML from the server and produce a UTBSResult.

    .. codeauthor:: Dean Rasheed (dev-group@ucs.cam.ac.uk)
    """
    def __init__(self):
        self.result = None
        self.node_stack = []
        self.parser = expat.ParserCreate()
        self.parser.StartElementHandler = self.start_element
        self.parser.EndElementHandler = self.end_element
        self.parser.CharacterDataHandler = self.char_data

    def start_element(self, tagname, attrs):
        element = None
        if self.node_stack:
            if tagname == "provider":
                element = UTBSProvider(attrs)
            elif tagname == "programme":
                element = UTBSProgramme(attrs)
            elif tagname == "currentProgramme":
                element = UTBSProgramme(attrs)
            elif tagname == "event":
                element = UTBSEvent(attrs)
            elif tagname == "course":
                element = UTBSCourse(attrs)
            elif tagname == "topic":
                element = UTBSEventTopic(attrs)
            elif tagname == "section":
                element = UTBSEventExtraInfo(attrs)
            elif tagname == "session":
                element = UTBSEventSession(attrs)
            elif tagname == "trainer":
                element = UTBSSessionTrainer(attrs)
            elif tagname == "theme":
                element = UTBSTheme(attrs)
            elif tagname == "venue":
                element = UTBSVenue(attrs)
            elif tagname == "person":
                element = UTBSPerson(attrs)
            elif tagname == "participant":
                element = UTBSPerson(attrs)
            elif tagname == "bookedBy":
                element = UTBSPerson(attrs)
            elif tagname == "booking":
                element = UTBSEventBooking(attrs)
            elif tagname == "sessionBooking":
                element = UTBSSessionBooking(attrs)
            elif tagname == "institution":
                element = UTBSInstitution(attrs)
            elif tagname == "parent":
                element = UTBSInstitution(attrs)
            elif tagname == "school":
                element = UTBSInstitution(attrs)
            elif tagname == "child":
                element = UTBSInstitution(attrs)
            elif tagname == "error":
                element = UTBSError(attrs)
            elif tagname == "entities":
                element = UTBSResult.Entities(attrs)
            else:
                parent = self.node_stack[-1]
                if (not isinstance(parent, list)) and\
                   (not isinstance(parent, dict)):
                    element = parent.start_child_element(tagname)
            if element == None:
                element = {"tagname": tagname}
        elif tagname != "result":
            raise Exception("Invalid root element: '%s'" % tagname)
        else:
            element = UTBSResult(attrs)
            self.result = element
        self.node_stack.append(element)

    def end_element(self, tagname):
        if self.node_stack:
            element = self.node_stack[-1]
            self.node_stack.pop()
            if self.node_stack:
                parent = self.node_stack[-1]
                if isinstance(parent, list):
                    if isinstance(element, dict):
                        parent.append(element.get("data"))
                    else:
                        parent.append(element)
                elif not isinstance(parent, dict):
                    if isinstance(element, dict):
                        data = element.get("data")
                    else:
                        data = element
                    parent.end_child_element(tagname, data)
        else:
            raise Exception("Unexpected closing tag: '%s'" % tagname)

    def char_data(self, data):
        if self.node_stack:
            element = self.node_stack[-1]
            if isinstance(element, UTBSEventTopic):
                if element.topic != None: element.topic += data
                else: element.topic = data
            elif isinstance(element, UTBSSessionTrainer):
                if element.name != None: element.name += data
                else: element.name = data
            elif isinstance(element, dict):
                if "data" in element: element["data"] += data
                else: element["data"] = data

    def parse_xml(self, data):
        """
        Parse XML data from the specified string and return a UTBSResult.

        **Parameters**
          `data` : str
            [required] The XML string returned from the server.

        **Returns**
          :any:`UTBSResult`
            The parsed results. This may contain lists or trees of objects
            representing providers, programmes, events, courses, themes,
            venues, people, bookings and institutions returned from the
            server.
        """
        self.parser.Parse(data)
        return self.result.unflatten()

    def parse_xml_file(self, file):
        """
        Parse XML data from the specified file and return a UTBSResult.

        **Parameters**
          `file` : file
            [required] A file object containing XML returned from the server.

        **Returns**
          :any:`UTBSResult`
            The parsed results. This may contain lists or trees of objects
            representing providers, programmes, events, courses, themes,
            venues, people, bookings and institutions returned from the
            server.
        """
        self.parser.ParseFile(file)
        return self.result.unflatten()
