#!/usr/bin/python

# --------------------------------------------------------------------------
# Copyright (c) 2013, University of Cambridge Computing Service.
#
# This file is part of the University Training Booking System client library.
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Dean Rasheed (dev-group@ucs.cam.ac.uk)
# --------------------------------------------------------------------------

from datetime import datetime
import re
import unittest

try:
    from utbsclient import *
except ImportError:
    import os
    import sys
    _script_dir = os.path.realpath(os.path.dirname(__file__))
    _utbsclient_dir = os.path.realpath(os.path.join(_script_dir, ".."))
    sys.path.append(_utbsclient_dir)
    from utbsclient import *

# Globals initialised once and re-used in each test
conn = None
um = None
provM = None
pm = None
em = None
cm = None
tm = None
vm = None
persM = None
bm = None
im = None
initialised = False

class UTBSUnitTests(unittest.TestCase):
    def setUp(self):
        global conn, um, provM, pm, em, cm, tm, vm, persM, bm, im
        global initialised

        if not initialised:
            conn = createLocalConnection()
            um = UTBSMethods(conn)
            provM = ProviderMethods(conn)
            pm = ProgrammeMethods(conn)
            em = EventMethods(conn)
            cm = CourseMethods(conn)
            tm = ThemeMethods(conn)
            vm = VenueMethods(conn)
            persM = PersonMethods(conn)
            bm = BookingMethods(conn)
            im = InstitutionMethods(conn)
            initialised = True
        conn.set_username(None)

    # --------------------------------------------------------------------
    # UTBS tests.
    # --------------------------------------------------------------------

    def test_get_version(self):
        self.assertEqual("1.1", um.getVersion())

    def test_get_current_user(self):
        self.assertIsNone(um.getCurrentUser())

        conn.set_username("dar17")
        self.assertEqual("dar17", um.getCurrentUser())

        conn.set_username(None)
        self.assertIsNone(um.getCurrentUser())

    def test_get_privileges(self):
        try:
            um.getPrivileges(None)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        conn.set_username("dar17")
        privs = um.getPrivileges(None)
        self.assertTrue("create-provider" in privs)

    # --------------------------------------------------------------------
    # Provider tests.
    # --------------------------------------------------------------------

    def test_no_such_provider(self):
        prov = provM.getProvider("sldkfnlks", None)
        self.assertIsNone(prov)

    def test_get_provider(self):
        prov = provM.getProvider("UIS", "themes,venues")
        self.assertEqual("ucs", prov.urlPrefix)
        self.assertEqual("UIS", prov.institution.instid)
        self.assertIsNotNone(prov.themes)
        self.assertIsNotNone(prov.venues)

        foundTheme = False
        for theme in prov.themes:
            if re.match(".*basic.*", theme.shortName) and\
               re.match(".*Basic.*Skills.*", theme.title): foundTheme = True
        self.assertTrue(foundTheme)

        foundVenue = False
        for venue in prov.venues:
            if venue.name == "New Museums Site, Hopkinson Lecture Theatre": foundVenue = True
        self.assertTrue(foundVenue)

        prov = provM.getProvider("UIS", "institution")
        self.assertEqual("University Information Services", prov.institution.name)

    def test_get_all_providers(self):
        provs = provM.getAllProviders("current_programme")
        self.assertIsNotNone(provs)
        self.assertTrue(len(provs) > 5)

        foundUCS = False
        for prov in provs:
            if prov.shortName == "UIS":
                self.assertIsNotNone(prov.currentProgramme)
                foundUCS = True
        self.assertTrue(foundUCS)

    def test_provider_get_programmes(self):
        prov = provM.getProvider("UIS", "programmes")
        progs = provM.getProgrammes("UIS", None)

        self.assertTrue(len(progs) > 5)
        self.assertEqual(len(progs), len(prov.programmes))
        for i in range(0, len(progs)):
            p1 = progs[i]
            p2 = prov.programmes[i]
            self.assertEqual(p1.id, p2.id)
            self.assertEqual(p1.shortName, p2.shortName)
            self.assertEqual(p1.title, p2.title)
            self.assertEqual(p1.startDate, p2.startDate)
            self.assertEqual(p1.endDate, p2.endDate)

        try:
            progs = provM.getProgrammes("UCSnosuchprovider", None)
            self.fail("Should have failed due to non-existent provider")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified training provider could not be found.", e.get_error().message)

    def test_no_such_prov_programmes_by_date(self):
        try:
            provM.getProgrammesByDate("UCSnosuchprovider", None, None, None)
            self.fail("Should have failed due to provider not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified training provider could not be found.", e.get_error().message)

    def test_prov_programmes_by_date(self):
        d1 = date(2013, 8, 30)
        d2 = date(2013, 8, 31)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(0, len(progs))

        d1 = date(2013, 8, 31)
        d2 = date(2013, 8, 31)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(0, len(progs))

        d1 = date(2013, 8, 31)
        d2 = date(2013, 9, 1)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(1, len(progs))
        self.assertEqual(1139396, progs[0].programmeId)

        d1 = date(2013, 9, 1)
        d2 = date(2013, 9, 1)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(1, len(progs))
        self.assertEqual(1139396, progs[0].programmeId)

        d1 = date(2014, 8, 29)
        d2 = date(2014, 8, 29)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(1, len(progs))
        self.assertEqual(1139396, progs[0].programmeId)

        d1 = date(2014, 8, 29)
        d2 = date(2014, 8, 30)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(1, len(progs))
        self.assertEqual(1139396, progs[0].programmeId)

        d1 = date(2014, 8, 30)
        d2 = date(2014, 8, 30)
        progs = provM.getProgrammesByDate("ENG", d1, d2)
        self.assertEqual(0, len(progs))

    def test_no_such_prov_events_in_time_period(self):
        try:
            provM.getEventsInTimePeriod("UCSnosuchprovider", None, None, False, None)
            self.fail("Should have failed due to provider not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified training provider could not be found.", e.get_error().message)

    def test_prov_events_in_time_period1(self):
        dt1 = datetime(2015, 3, 23, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 23, 9, 29, 59, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 23, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 23, 9, 30, 0, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 0, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 1, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 1, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 2, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_prov_events_in_time_period2(self):
        dt1 = datetime(2015, 3, 11, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 11, 9, 29, 59, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 11, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 11, 9, 30, 0, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 0, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 1, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 1, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 2, 0)
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = provM.getEventsInTimePeriod("BIOINFO", dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    def test_provider_get_themes(self):
        themes = provM.getThemes("UIS")
        self.assertTrue(len(themes) > 30)
        self.assertEqual("assist-tech", themes[0].shortName)
        self.assertEqual("Assistive Technology", themes[0].title)

        try:
            themes = provM.getThemes("UCSnosuchprovider")
            self.fail("Should have failed due to non-existent provider")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified training provider could not be found.", e.get_error().message)

    def test_provider_get_venues(self):
        venues = provM.getVenues("BIOINFO")
        self.assertTrue(len(venues) >= 5)
        self.assertEqual(1122015, venues[0].venueId)
        self.assertEqual("Craik-Marshall", venues[0].shortName)
        self.assertEqual("Bioinformatics Training Room, Craik-Marshall Building, Downing Site", venues[0].name)

        try:
            venues = provM.getVenues("UCSnosuchprovider")
            self.fail("Should have failed due to non-existent provider")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified training provider could not be found.", e.get_error().message)

    # --------------------------------------------------------------------
    # Programme tests.
    # --------------------------------------------------------------------

    def test_no_such_programme(self):
        p = pm.getProgramme(-123, None)
        self.assertIsNone(p)

    def test_get_programme(self):
        p = pm.getProgramme(1866496, "provider,events")
        self.assertEqual(1866496, p.programmeId)
        self.assertEqual("ucs-f2f-ay2015-2016", p.shortName)
        self.assertEqual("UCS IT Skills Training 2015-2016", p.title)

        self.assertEqual("ucs", p.provider.urlPrefix)
        self.assertEqual("UIS", p.provider.institution.instid)

        self.assertIsNotNone(p.events)
        self.assertTrue(len(p.events) > 50)

        foundEvent = False
        for event in p.events:
            if event.eventId == 1536631 and\
               event.courseType == "instructor-led" and\
               re.match("Web Authoring.*", event.title): foundEvent = True
        self.assertTrue(foundEvent)

    def test_get_programmes_by_date(self):
        d1 = date(2008, 8, 30)
        d2 = date(2008, 8, 31)
        progs = pm.getProgrammesByDate(d1, d2)
        self.assertEqual(0, len(progs))

        d1 = date(2008, 8, 31)
        d2 = date(2008, 8, 31)
        progs = pm.getProgrammesByDate(d1, d2)
        self.assertEqual(0, len(progs))

        d1 = date(2008, 8, 31)
        d2 = date(2008, 9, 1)
        progs = pm.getProgrammesByDate(d1, d2)
        self.assertEqual(2, len(progs))
        self.assertEqual(1867631, progs[0].programmeId)
        self.assertEqual(2384821, progs[1].programmeId)

        d1 = date(2008, 9, 1)
        d2 = date(2008, 9, 1)
        progs = pm.getProgrammesByDate(d1, d2)
        self.assertEqual(2, len(progs))
        self.assertEqual(1867631, progs[0].programmeId)
        self.assertEqual(2384821, progs[1].programmeId)

        d1 = date(2011, 11, 11)
        d2 = date(2011, 11, 11)
        progs = pm.getProgrammesByDate(d1, d2)
        self.assertEqual(27, len(progs))
        self.assertEqual(183144, progs[0].programmeId)
        self.assertEqual(227599, progs[1].programmeId)
        self.assertEqual(228037, progs[2].programmeId)

    def test_no_such_prog_events_in_time_period(self):
        try:
            pm.getEventsInTimePeriod(-123, None, None, False, None)
            self.fail("Should have failed due to programme not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified programme could not be found.", e.get_error().message)

    def test_prog_events_in_time_period1(self):
        dt1 = datetime(2015, 3, 23, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 23, 9, 29, 59, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 23, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 23, 9, 30, 0, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 0, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 1, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 1, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 2, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_prog_events_in_time_period2(self):
        dt1 = datetime(2015, 3, 11, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 11, 9, 29, 59, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 11, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 11, 9, 30, 0, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 0, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 1, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 1, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 2, 0)
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = pm.getEventsInTimePeriod(1372103, dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    def test_no_such_prog_self_taught_events(self):
        try:
            pm.getSelfTaughtEvents(-123, None)
            self.fail("Should have failed due to programme not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified programme could not be found.", e.get_error().message)

    def test_prog_self_taught_events(self):
        events = pm.getSelfTaughtEvents(1867620, None)
        self.assertEqual(0, len(events))

        events = pm.getSelfTaughtEvents(1249349, "sessions,topics")
        self.assertEqual(201, len(events))
        self.assertEqual(1249410, events[0].eventId)
        self.assertEqual("Access 2007: Level 1 (Win) (Workbooks)", events[0].title)
        self.assertEqual(0, len(events[0].sessions))
        self.assertIsNone(events[0].topics[0].sessionNumber)
        self.assertTrue("Access 2007 Orientation" in events[0].topics[0].topic)

        conn.set_username("dar17")
        events = pm.getSelfTaughtEvents(1249349, None)
        self.assertEqual(209, len(events))

    # --------------------------------------------------------------------
    # Event tests.
    # --------------------------------------------------------------------

    def test_no_such_event(self):
        e = em.getEvent(-123, None)
        self.assertIsNone(e)

    def test_get_event(self):
        e = em.getEvent(1536088, "provider,programme,sessions.venue,sessions.trainers")
        self.assertEqual(1536088, e.eventId)
        self.assertEqual("Excel 2010/2013: Introduction", e.title)
        self.assertEqual("instructor-led", e.courseType)

        self.assertEqual("UIS", e.provider.shortName)
        self.assertEqual("University Information Services", e.provider.name)

        self.assertEqual(1866496, e.programme.programmeId)
        self.assertEqual("ucs-f2f-ay2015-2016", e.programme.shortName)
        self.assertEqual("UCS IT Skills Training 2015-2016", e.programme.title)

        self.assertIsNotNone(e.sessions)
        self.assertEqual(2, len(e.sessions))

        self.assertEqual("UISRNBELY1", e.sessions[0].venue.shortName)
        self.assertEqual("UISRNBELY2", e.sessions[1].venue.shortName)

        self.assertEqual(2, len(e.sessions[0].trainers))
        self.assertEqual(2, len(e.sessions[1].trainers))

        self.assertEqual("ltf23", e.sessions[0].trainers[0].crsid)
        self.assertEqual("ph441", e.sessions[0].trainers[1].crsid)
        self.assertEqual("Lynn Foot", e.sessions[1].trainers[0].name)
        self.assertEqual("Paul Hunt-Deol", e.sessions[1].trainers[1].name)

        e = em.getEvent(1536088, "course.events,related_courses.events")
        self.assertTrue(len(e.course.events) > 26)
        self.assertEqual(124, e.course.events[0].eventId)
        self.assertTrue(len(e.relatedCourses) > 10)
        self.assertEqual("ucs-excelbeg", e.relatedCourses[0].courseId)
        self.assertTrue(len(e.relatedCourses[0].events) > 41)
        self.assertEqual(56, e.relatedCourses[0].events[0].eventId)

        e = em.getEvent(1536088, "themes,venues")
        self.assertEqual(1, len(e.themes))
        self.assertEqual(2, len(e.venues))
        self.assertEqual("Spreadsheets", e.themes[0].title)
        self.assertEqual("UISRNBELY1", e.venues[0].shortName)
        self.assertEqual("UISRNBELY2", e.venues[1].shortName)

    def test_get_event_topics(self):
        e = em.getEvent(1536088, "topics")
        self.assertEqual(1536088, e.eventId)
        self.assertEqual(1, len(e.topics))

        t = e.topics[0]
        self.assertTrue("Building a new workbook" in t.topic)

    def test_get_event_extra_info(self):
        e = em.getEvent(1536088, "extra_info")
        self.assertEqual(1536088, e.eventId)
        self.assertEqual(1, len(e.extraInfo))

        ei = e.extraInfo[0]
        self.assertEqual("Taught using", ei.heading)
        self.assertTrue("Excel 2010" in ei.content)

    def test_get_event_trainers(self):
        e = em.getEvent(1536088, "trainers")
        self.assertEqual(1536088, e.eventId)
        self.assertEqual(2, len(e.trainers))

        t1 = e.trainers[0]
        self.assertEqual("ltf23", t1.crsid)
        self.assertEqual("Lynn Foot", t1.name)

        t2 = e.trainers[1]
        self.assertEqual("ph441", t2.crsid)
        self.assertEqual("Paul Hunt-Deol", t2.name)

    def test_get_events_in_time_period1(self):
        dt1 = datetime(2009, 5, 15, 9, 29, 58, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 15, 9, 29, 59, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2009, 5, 15, 9, 29, 59, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 15, 9, 30, 0, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(2146, events[0].eventId)

        dt1 = datetime(2009, 5, 15, 13, 0, 0, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 15, 13, 0, 1, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(2146, events[0].eventId)

        dt1 = datetime(2009, 5, 15, 13, 0, 1, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 15, 13, 0, 2, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_get_events_in_time_period2(self):
        dt1 = datetime(2009, 5, 18, 9, 29, 58, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 18, 9, 29, 59, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2009, 5, 18, 9, 29, 59, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 18, 9, 30, 0, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(2156, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2009, 5, 18, 12, 30, 0, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 18, 12, 30, 1, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(2156, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2009, 5, 18, 12, 30, 1, 0, TimeZone(hours=1))
        dt2 = datetime(2009, 5, 18, 12, 30, 2, 0, TimeZone(hours=1))
        events = em.getEventsInTimePeriod(dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = em.getEventsInTimePeriod(dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(2156, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    def test_no_such_event_bookings(self):
        try:
            em.getBookings(-123)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            em.getBookings(-123)
            self.fail("Should have failed due to event not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified event could not be found.", e.get_error().message)

    def test_get_event_bookings(self):
        try:
            em.getBookings(150453, None)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            em.getBookings(150453, None)
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to view the bookings for this event", e.get_error().message)

        conn.set_username("dar17")
        bookings = em.getBookings(150453, "session_bookings")
        self.assertTrue(len(bookings) >= 10)

        booking = None
        for b in bookings:
            if b.participant.crsid == "mg356": booking = b

        self.assertIsNotNone(booking)
        self.assertEqual(154239, booking.bookingId)
        self.assertEqual("mg356", booking.bookedBy.crsid)
        self.assertEqual(2, len(booking.sessionBookings))
        self.assertTrue(booking.sessionBookings[0].booked)
        self.assertTrue(booking.sessionBookings[1].booked)

    def test_event_fetch_bookings(self):
        e = em.getEvent(10427, "bookings")
        self.assertEqual(0, len(e.bookings))

        conn.set_username("aa494")
        e = em.getEvent(10427, "bookings")
        self.assertEqual(1, len(e.bookings))
        self.assertEqual("aa494", e.bookings[0].participant.crsid)

        conn.set_username("dar17")
        e = em.getEvent(10427, "bookings")
        self.assertEqual(33, len(e.bookings))
        self.assertEqual("aa494", e.bookings[0].participant.crsid)

    def test_record_attendance(self):
        try:
            em.recordAttendance(-123, 3, "mg356", False)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            em.recordAttendance(-123, 3, "mg356", False)
            self.fail("Should have failed due to event not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified event could not be found.", e.get_error().message)

        try:
            conn.set_username("dar99")
            em.recordAttendance(150453, 3, "mg356", False)
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to record attendance for this event", e.get_error().message)

        try:
            conn.set_username("dar17")
            em.recordAttendance(150453, 3, "mg356", False)
            self.fail("Should have failed due to session not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified session could not be found.", e.get_error().message)

        conn.set_username("dar17")
        b = bm.getBooking(154239, "session_bookings")
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, 1, False)
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

        b = em.recordAttendance(150453, 1, "mg356", False)
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

        b = bm.getBooking(154239, "session_bookings")
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

    # --------------------------------------------------------------------
    # Course tests.
    # --------------------------------------------------------------------

    def test_no_such_course(self):
        c = cm.getCourse("lsiegf3w4in", None)
        self.assertIsNone(c)

    def test_get_course(self):
        c = cm.getCourse("ucs-progbasics", "events,themes")
        self.assertEqual("ucs-progbasics", c.courseId)

        self.assertIsNotNone(c.events)
        self.assertTrue(len(c.events) >= 5)

        e = c.events[0]
        self.assertEqual(46, e.eventId)
        self.assertEqual("Programming Concepts: Introduction for Absolute Beginners", e.title)
        self.assertEqual("instructor-led", e.courseType)

        self.assertIsNotNone(c.themes)
        self.assertEqual(4, len(c.themes))

        foundTheme = False
        for theme in c.themes:
            if theme.shortName == "programming-script" and\
               theme.title == "Programming and Scripting": foundTheme = True
        self.assertTrue(foundTheme)

    def test_no_such_course_events_in_time_period(self):
        try:
            cm.getEventsInTimePeriod("lsiegf3w4in", None, None, False, None)
            self.fail("Should have failed due to course not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified course could not be found.", e.get_error().message)

    def test_course_events_in_time_period1(self):
        dt1 = datetime(2015, 3, 23, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 23, 9, 29, 59, 0)
        events = cm.getEventsInTimePeriod("bioinfo-ensbrow", dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 23, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 23, 9, 30, 0, 0)
        events = cm.getEventsInTimePeriod("bioinfo-ensbrow", dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 0, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 1, 0)
        events = cm.getEventsInTimePeriod("bioinfo-ensbrow", dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 1, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 2, 0)
        events = cm.getEventsInTimePeriod("bioinfo-ensbrow", dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_course_events_in_time_period2(self):
        dt1 = datetime(2015, 3, 11, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 11, 9, 29, 59, 0)
        events = cm.getEventsInTimePeriod("bioinfo-rintro", dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 11, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 11, 9, 30, 0, 0)
        events = cm.getEventsInTimePeriod("bioinfo-rintro", dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 0, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 1, 0)
        events = cm.getEventsInTimePeriod("bioinfo-rintro", dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 1, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 2, 0)
        events = cm.getEventsInTimePeriod("bioinfo-rintro", dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = cm.getEventsInTimePeriod("bioinfo-rintro", dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    def test_no_such_course_self_taught_events(self):
        try:
            cm.getSelfTaughtEvents("lsiegf3w4in", None)
            self.fail("Should have failed due to course not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified course could not be found.", e.get_error().message)

    def test_course_self_taught_events(self):
        events = cm.getSelfTaughtEvents("fin-ol-ch")
        self.assertEqual("Cash Handling", events[0].title)

    # --------------------------------------------------------------------
    # Theme tests.
    # --------------------------------------------------------------------

    def test_get_all_themes(self):
        themes = tm.getAllThemes()
        self.assertTrue(len(themes) > 200)
        self.assertEqual("acadpublish", themes[0].shortName)
        self.assertEqual("Academic Publishing", themes[0].title)

    def test_no_such_theme(self):
        t = tm.getTheme(-123, None)
        self.assertIsNone(t)

    def test_get_theme(self):
        t = tm.getTheme(36446, "provider.institution,events")
        self.assertEqual(36446, t.themeId)
        self.assertEqual("graphics-photo", t.shortName)
        self.assertEqual("Graphics and Photos", t.title)

        self.assertEqual("ucs", t.provider.urlPrefix)
        self.assertEqual("UIS", t.provider.institution.instid)
        self.assertEqual("University Information Services", t.provider.institution.name)

        self.assertIsNotNone(t.events)
        self.assertTrue(len(t.events) >= 11)

        foundEvent = False
        for e in t.events:
            if e.title == "Photoshop: Basic Techniques" and\
               e.courseType == "instructor-led": foundEvent = True
        self.assertTrue(foundEvent)

        t = tm.getTheme(36446, "courses.events")

        self.assertIsNotNone(t.courses)
        self.assertTrue(len(t.courses) >= 10)

        course = None
        for c in t.courses:
            if c.courseId == "ucs-photoshop": course = c
        self.assertIsNotNone(course)

        foundEvent = False
        for e in course.events:
            if e.title == "Photoshop: Basic Techniques" and\
               e.courseType == "instructor-led": foundEvent = True
        self.assertTrue(foundEvent)

    def test_get_themes_by_name(self):
        themes = tm.getThemesByName("presentation", None, "provider")
        self.assertEqual(4, len(themes))
        self.assertEqual(177091, themes[0].themeId)
        self.assertEqual("Presentation & Communication", themes[0].title)
        self.assertEqual("GSLS", themes[0].provider.shortName)

        themes = tm.getThemesByName("presentation", "UIS", None)
        self.assertEqual(1, len(themes))
        self.assertEqual(36440, themes[0].themeId)

    def test_no_such_theme_events_in_time_period(self):
        try:
            tm.getEventsInTimePeriod(-123, None, None, False, None)
            self.fail("Should have failed due to theme not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified theme could not be found.", e.get_error().message)

    def test_theme_events_in_time_period1(self):
        dt1 = datetime(2015, 3, 23, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 23, 9, 29, 59, 0)
        events = tm.getEventsInTimePeriod(1355174, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 23, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 23, 9, 30, 0, 0)
        events = tm.getEventsInTimePeriod(1355174, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 0, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 1, 0)
        events = tm.getEventsInTimePeriod(1355174, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1351263, events[0].eventId)

        dt1 = datetime(2015, 3, 23, 17, 0, 1, 0)
        dt2 = datetime(2015, 3, 23, 17, 0, 2, 0)
        events = tm.getEventsInTimePeriod(1355174, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_theme_events_in_time_period2(self):
        dt1 = datetime(2015, 3, 11, 9, 29, 58, 0)
        dt2 = datetime(2015, 3, 11, 9, 29, 59, 0)
        events = tm.getEventsInTimePeriod(1355176, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 3, 11, 9, 29, 59, 0)
        dt2 = datetime(2015, 3, 11, 9, 30, 0, 0)
        events = tm.getEventsInTimePeriod(1355176, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 0, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 1, 0)
        events = tm.getEventsInTimePeriod(1355176, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 3, 11, 17, 30, 1, 0)
        dt2 = datetime(2015, 3, 11, 17, 30, 2, 0)
        events = tm.getEventsInTimePeriod(1355176, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = tm.getEventsInTimePeriod(1355176, dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1346856, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    def test_no_such_theme_self_taught_events(self):
        try:
            tm.getSelfTaughtEvents(-123, None)
            self.fail("Should have failed due to theme not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified theme could not be found.", e.get_error().message)

    def test_theme_self_taught_events(self):
        events = tm.getSelfTaughtEvents(36450)
        self.assertEqual("Introduction to Personal Computers Using Windows 8.1 and Microsoft Office 2013", events[0].title)

    # --------------------------------------------------------------------
    # Venue tests.
    # --------------------------------------------------------------------

    def test_get_all_venues(self):
        venues = vm.getAllVenues()
        self.assertTrue(len(venues) > 600)
        self.assertEqual("16MLEAST", venues[0].shortName)
        self.assertEqual("16 Mill Lane, Office of Post-Doctoral Affairs, Eastwood Room", venues[0].name)
        self.assertEqual("ZOOMUEH", venues[-1].shortName)
        self.assertEqual("Zoology Museum, Exhibition Hall", venues[-1].name)

    def test_no_such_venue(self):
        v = vm.getVenue(-123, None)
        self.assertIsNone(v)

    def test_get_venue(self):
        v = vm.getVenue(6, "provider")
        self.assertEqual(6, v.venueId)
        self.assertEqual("NMSHOPK", v.shortName)
        self.assertEqual("New Museums Site, Hopkinson Lecture Theatre", v.name)
        self.assertIsNone(v.capacity)

        self.assertEqual("ucs", v.provider.urlPrefix)
        self.assertEqual("UIS", v.provider.institution.instid)

    def test_no_such_venue_events_in_time_period(self):
        try:
            vm.getEventsInTimePeriod(-123, None, None, False, None)
            self.fail("Should have failed due to venue not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified venue could not be found.", e.get_error().message)

    def test_venue_events_in_time_period1(self):
        dt1 = datetime(2015, 10, 12, 13, 59, 58, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 12, 13, 59, 59, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 10, 12, 13, 59, 59, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 12, 14, 0, 0, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1535799, events[0].eventId)

        dt1 = datetime(2015, 10, 12, 16, 15, 0, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 12, 16, 15, 1, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, False, None)
        self.assertEqual(1, len(events))
        self.assertEqual(1535799, events[0].eventId)

        dt1 = datetime(2015, 10, 12, 16, 15, 1, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 12, 16, 15, 2, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, False, None)
        self.assertEqual(0, len(events))

    def test_venue_events_in_time_period2(self):
        dt1 = datetime(2015, 10, 20, 13, 59, 58, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 20, 13, 59, 59, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))

        dt1 = datetime(2015, 10, 20, 13, 59, 59, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 20, 14, 0, 0, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1536163, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 10, 20, 17, 0, 0, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 20, 17, 0, 1, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, True, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1536163, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

        dt1 = datetime(2015, 10, 20, 17, 0, 1, 0, TimeZone(hours=1))
        dt2 = datetime(2015, 10, 20, 17, 0, 2, 0, TimeZone(hours=1))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, True, "sessions")
        self.assertEqual(0, len(events))
        events = vm.getEventsInTimePeriod(6, dt1, dt2, False, "sessions")
        self.assertEqual(1, len(events))
        self.assertEqual(1536163, events[0].eventId)
        self.assertEqual(2, len(events[0].sessions))

    # --------------------------------------------------------------------
    # Person tests.
    # --------------------------------------------------------------------

    def test_no_such_person(self):
        try:
            persM.getPerson("xxx")
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        conn.set_username("dar17")
        p = persM.getPerson("xxx")
        self.assertIsNone(p)

    def test_get_person(self):
        try:
            persM.getPerson("dar17")
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            persM.getPerson("dar17")
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to view this person", e.get_error().message)

        conn.set_username("dar17")
        p = persM.getPerson("dar17")
        self.assertEqual("dar17", p.crsid)
        self.assertEqual("Dean Rasheed", p.name)
        self.assertEqual("dar17@cam.ac.uk", p.email)

    def test_get_training_history(self):
        try:
            persM.getTrainingHistory("dar17")
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            persM.getTrainingHistory("dar17")
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to view this person's bookings", e.get_error().message)

        conn.set_username("dar17")
        bookings = persM.getTrainingHistory("dar17", "event")
        self.assertTrue(len(bookings) >= 5)
        self.assertEqual(225520, bookings[0].bookingId)
        self.assertEqual(214585, bookings[0].event.eventId)
        self.assertEqual("UCS: A Practical Introduction to Falcon for UCS Members", bookings[0].event.title)

        conn.set_username("dar99")
        bookings = persM.getTrainingHistory("dar99", None)
        self.assertEquals(0, len(bookings))

    # --------------------------------------------------------------------
    # Booking tests.
    # --------------------------------------------------------------------

    def test_no_such_booking(self):
        try:
            bm.getBooking(1, None)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        conn.set_username("dar17")
        b = bm.getBooking(1, None)
        self.assertIsNone(b)

    def test_get_booking(self):
        try:
            bm.getBooking(1914099, None)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            bm.getBooking(1914099, None)
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to view this booking", e.get_error().message)

        conn.set_username("dar17")
        b1 = bm.getBooking(1914099, "participant,booked_by,institutions")
        self.assertEqual(1914099, b1.bookingId)
        self.assertEqual(1871672, b1.event.eventId)
        self.assertIsNone(b1.event.title)
        self.assertEqual("rjd4", b1.participant.crsid)
        self.assertEqual("Bob Dowling", b1.participant.name)
        self.assertEqual("mg356", b1.bookedBy.crsid)
        self.assertEqual("Monica Gonzalez", b1.bookedBy.name)
        self.assertEqual("2016-09-23T17:46:42.479000+01:00", b1.bookingTime.isoformat())
        self.assertEqual("UIS", b1.institution.instid)
        self.assertIsNone(b1.institution.name)
        self.assertEqual(1, len(b1.institutions))
        self.assertEqual("UIS", b1.institutions[0].instid)
        self.assertEqual("University Information Services", b1.institutions[0].name)

        b2 = bm.getBooking(154239, "event,participant,institution,session_bookings")
        self.assertEqual(154239, b2.bookingId)
        self.assertEqual(150453, b2.event.eventId)
        self.assertEqual("UCS: A Practical Introduction to Falcon for UCS Members", b2.event.title)
        self.assertEqual("mg356", b2.participant.crsid)
        self.assertEqual("Monica Gonzalez", b2.participant.name)
        self.assertEqual("mg356", b2.bookedBy.crsid)
        self.assertIsNone(b2.bookedBy.name)
        self.assertEqual("CS", b2.institution.instid)
        self.assertEqual("University Computing Service", b2.institution.name)
        self.assertTrue(b2.attended)
        self.assertIsNone(b2.passed)
        self.assertEqual(2, len(b2.sessionBookings))

        sb1 = b2.sessionBookings[0]
        self.assertEqual(1, sb1.sessionNumber)
        self.assertTrue(sb1.booked)
        self.assertTrue(sb1.attended)
        self.assertIsNone(sb1.passed)

        sb2 = b2.sessionBookings[1]
        self.assertEqual(2, sb2.sessionNumber)
        self.assertTrue(sb2.booked)
        self.assertTrue(sb2.attended)
        self.assertIsNone(sb2.passed)

    def test_update_booking_attendance(self):
        try:
            bm.updateAttendance(-123, None, True)
            self.fail("Should have failed due to not being authenticated")
        except UTBSException as e:
            self.assertEqual(401, e.get_error().status)
            self.assertEqual("This method requires authentication.", e.get_error().message)

        try:
            conn.set_username("dar99")
            bm.updateAttendance(-123, None, True)
            self.fail("Should have failed due to booking not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified booking could not be found.", e.get_error().message)

        try:
            conn.set_username("dar99")
            bm.updateAttendance(154239, None, True)
            self.fail("Should have failed due to insufficient privileges")
        except UTBSException as e:
            self.assertEqual(403, e.get_error().status)
            self.assertEqual("You do not have permission to record attendance for this event", e.get_error().message)

        try:
            conn.set_username("dar17")
            bm.updateAttendance(154239, 3, True)
            self.fail("Should have failed due to session not existing")
        except UTBSException as e:
            self.assertEqual(404, e.get_error().status)
            self.assertEqual("The specified session could not be found.", e.get_error().message)

        conn.set_username("dar17")
        b = bm.getBooking(154239, "session_bookings")
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, 1, False)
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, 2, None)
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, 1, True)
        self.assertIsNone(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertIsNone(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, None, None)
        self.assertIsNone(b.attended)
        self.assertIsNone(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertIsNone(b.attended)
        self.assertIsNone(b.sessionBookings[0].attended)
        self.assertIsNone(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, None, False)
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertFalse(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertFalse(b.attended)
        self.assertFalse(b.sessionBookings[0].attended)
        self.assertFalse(b.sessionBookings[1].attended)

        b = bm.updateAttendance(154239, None, True)
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)
        b = bm.getBooking(154239, "session_bookings")
        self.assertTrue(b.attended)
        self.assertTrue(b.sessionBookings[0].attended)
        self.assertTrue(b.sessionBookings[1].attended)

    # --------------------------------------------------------------------
    # Institution tests.
    # --------------------------------------------------------------------

    def test_no_such_institution(self):
        i = im.getInstitution("XXX", None)
        self.assertIsNone(i)

    def test_get_institution(self):
        i = im.getInstitution("UIS", "parent.children")
        self.assertEqual("UIS", i.instid)
        self.assertEqual("University Information Services", i.name)
        self.assertEqual("INSC", i.parent.instid)
        self.assertEqual("Institutions Not Under the Supervision of the General Board", i.parent.name)
        self.assertEqual("INSC", i.school.instid)
        self.assertIsNone(i.school.name)

        foundInst = False
        for ii in i.parent.children:
            if ii.instid == "ADCTH" and\
               ii.name == "ADC Theatre": foundInst = True
        self.assertTrue(foundInst)

        i = im.getInstitution("PHY", "school")
        self.assertEqual("PHY", i.instid)
        self.assertEqual("Department of Physics", i.name)
        self.assertEqual("IUSCPHY", i.school.instid)
        self.assertEqual("Institutions under the School of the Physical Sciences", i.school.name)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(UTBSUnitTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
