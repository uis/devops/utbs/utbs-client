/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.example;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.*;
import uk.ac.cam.ucs.utbs.dto.*;
import uk.ac.cam.ucs.utbs.methods.*;

/**
 * Example class to demonstrate how to use the UTBS web service client API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class Example
{
    /**
     * Run the example.
     */
    public static void main(String[]    args)
        throws GeneralSecurityException, UTBSException, IOException,
               JAXBException
    {
        // The following setup need only be run once. First create a
        // ClientConnection object that can connect to the UTBS server.
        // This is not a persistent connection, but rather it automatically
        // connects to the server as needed.
        //
        // To connect to the live server on www.training.cam.ac.uk use:
        // ClientConnection conn = UTBSClientConnection.createConnection();
        // or to connect to the test server on utbs-test.csx.cam.ac.uk:
        ClientConnection conn = UTBSClientConnection.createTestConnection();

        // Create instances of the method classes, depending on which
        // API methods you require. Again this need only be done once.
        EventMethods em = new EventMethods(conn);

        // Invoke a web service method (this will fetch a specific event and
        // all of its sessions and topics)
        UTBSEvent e = em.getEvent(58900L, "sessions,topics");

        // Print the results
        System.out.println("");
        System.out.println("Event: "+e.title);
        System.out.println("");
        System.out.println("Description");
        System.out.println("===========");
        System.out.println(e.description);
        System.out.println("");
        System.out.println("Topics covered");
        System.out.println("==============");
        System.out.println(e.topics.get(0).topic);
        System.out.println("");
        for (UTBSEventSession s : e.sessions)
        {
            System.out.println("Session "+s.sessionNumber+":");
            System.out.println("  "+s.startTime+" - "+s.endTime);
        }
    }
}
