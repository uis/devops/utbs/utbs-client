/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating bookings.
 *
 * <h4>The fetch parameter for event bookings</h4>
 * <p>
 * All methods that return bookings also accept an optional
 * <code>fetch</code> parameter that may be used to request additional
 * information about the bookings returned. For more details about the
 * general rules that apply to the <code>fetch</code> parameter, refer to the
 * {@link EventMethods} documentation.
 * <p>
 * For bookings the <code>fetch</code> parameter may be used to fetch details
 * about the individual sessions booked and the referenced event, people and
 * institutions:
 * <ul>
 * <li>{@code "event"} - fetches details about the event booked.</li>
 * <li>{@code "participant"} - fetches details about the person the booking
 * is for.</li>
 * <li>{@code "booked_by"} - fetches details about the person who made the
 * booking.</li>
 * <li>{@code "institution"} - fetches details about the participant's
 * institution chosen at the time the booking was made.</li>
 * <li>{@code "session_bookings"} - fetches the session booking details.</li>
 * <li>{@code "institutions"} - fetches a list of all the participant's
 * institutions, as they were at the time the booking was made.</li>
 * </ul>
 * <p>
 * As with the event <code>fetch</code> parameter, this reference may be used
 * in a chain by using the "dot" notation to fetch additional information
 * about the referenced event or institutions. For example
 * "institution.school" will fetch the participant's chosen institution and
 * the school to which it belongs. For more information about what can be
 * fetched from the referenced event or institutions, refer to the
 * documentation for {@link EventMethods} and {@link InstitutionMethods}.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class BookingMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new BookingMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public BookingMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get the event booking with the specified ID.
     * <p>
     * By default, only a few basic details about the booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * Note that viewing bookings requires authentication, and a booking is
     * only visible to the following:
     * <ul>
     * <li>The participant.</li>
     * <li>The person who made the booking (not necessarily the
     * participant).</li>
     * <li>The event's trainers.</li>
     * <li>The event's administrator, owner and booker.</li>
     * <li>People with the "view-event-bookings" privilege for the event's
     * provider.</li>
     * <li>People with the "view-3rd-party-booking" privilege for the event's
     * provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/booking/{id} ]</code>
     *
     * @param id [required] The ID of the booking to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested booking or null if it was not found.
     */
    public UTBSEventBooking getBooking(Long     id,
                                       String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/booking/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }

    /**
     * Update an event booking to record whether or not the participant
     * attended the event.
     * <p>
     * The attendance may be updated for a single session on the event, by
     * specifying a particular session number, or for all booked sessions, by
     * passing {@code null} as the session number.
     * <p>
     * The attendance may be set to {@code true} or {@code false} to indicate
     * whether or not the participant attended, or it may be set to
     * {@code null} (its original value) to indicate that their attendance
     * has not yet been recorded, or is currently unknown.
     * <p>
     * Note that updating attendance requires authentication as a user with
     * permission to view the booking (see
     * {@link #getBooking(Long,String) getBooking()}) and record attendance
     * on the event. So in addition to being able to view the booking, the
     * user must be either one of the following:
     * <ul>
     * <li>The event administrator.</li>
     * <li>A person with the "record-attendance" privilege for the event's
     * provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/booking/{id}/attendance ]</code>
     *
     * @param id [required] The ID of the booking to update.
     * @param sessionNumber [optional] The session to update the attendance
     * for, or {@code null} to update all booked sessions (the default).
     * @param attended [optional] Whether or not the participant attended, or
     * {@code null} to record that their attendance has not been set (the
     * default).
     *
     * @return The updated booking, together with all associated session
     * bookings.
     */
    public UTBSEventBooking updateAttendance(Long       id,
                                             Integer    sessionNumber,
                                             Boolean    attended)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = {  };
        Object[] formParams = { "sessionNumber", sessionNumber,
                                "attended", attended };
        UTBSResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/booking/%1$s/attendance",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }

    /**
     * Update an event booking to record whether or not the participant
     * attended the event, and whether they passed or failed.
     * <p>
     * The attendance and outcome may be updated for a single session on the
     * event, by specifying a particular session number, or for all booked
     * sessions, by passing {@code null} as the session number.
     * <p>
     * The attendance and outcome may each be set to {@code true} or
     * {@code false} to indicate whether or not the participant attended or
     * passed, or it may be set to {@code null} (its original value) to
     * indicate that their attendance/outcome has not yet been recorded, or
     * is currently unknown.
     * <p>
     * Note that updating attendance/outcome requires authentication as a user
     * with permission to view the booking (see
     * {@link #getBooking(Long,String) getBooking()}) and record attendance
     * on the event. So in addition to being able to view the booking, the
     * user must be either one of the following:
     * <ul>
     * <li>The event administrator.</li>
     * <li>A person with the "record-attendance" privilege for the event's
     * provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/booking/{id}/outcome ]</code>
     *
     * @param id [required] The ID of the booking to update.
     * @param sessionNumber [optional] The session to update the attendance
     * for, or {@code null} to update all booked sessions (the default).
     * @param attended [optional] Whether or not the participant attended, or
     * {@code null} to record that their attendance has not been set (the
     * default).
     * @param passed [optional] Whether or not the participant passed, or
     * {@code null} to record that their outcome has not been set (the
     * default).     
     *
     * @return The updated booking, together with all associated session
     * bookings.
     */
    public UTBSEventBooking updateOutcome(Long      id,
                                          Integer   sessionNumber,
                                          Boolean   attended,
                                          Boolean   passed)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = {  };
        Object[] formParams = { "sessionNumber", sessionNumber,
                                "attended", attended,
                                "passed", passed };
        UTBSResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/booking/%1$s/outcome",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }
}
