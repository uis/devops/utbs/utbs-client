/* === AUTO-GENERATED - DO NOT EDIT === */

/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.methods;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import uk.ac.cam.ucs.utbs.client.ClientConnection;
import uk.ac.cam.ucs.utbs.client.ClientConnection.Method;
import uk.ac.cam.ucs.utbs.client.UTBSException;
import uk.ac.cam.ucs.utbs.dto.*;

/**
 * Methods for querying and manipulating events.
 *
 * <h4>The fetch parameter for events</h4>
 * <p>
 * All methods that return events also accept an optional <code>fetch</code>
 * parameter that may be used to request additional information about the
 * events returned. The following additional information may be fetched:
 * <ul>
 * <li>{@code "provider"} - fetches the event's provider.</li>
 * <li>{@code "programme"} - fetches the programme containing the event.</li>
 * <li>{@code "course"} - fetches the event's course, from which other events
 * for the same course may be fetched by specifying
 * {@code "course.events"}.</li>
 * <li>{@code "topics"} - fetches the event's topics sections.</li>
 * <li>{@code "extra_info"} - fetches any extra information sections that the
 * event has.</li>
 * <li>{@code "related_courses"} - fetches any related courses for the event,
 * from which related events may be fetched by specifying
 * {@code "related_courses.events"}.</li>
 * <li>{@code "themes"} - fetches the event's themes.</li>
 * <li>{@code "sessions"} - fetches the event's sessions.</li>
 * <li>{@code "sessions.trainers"} - fetches the event's sessions and the
 * trainers for each session.</li>
 * <li>{@code "sessions.venue"} - fetches the event's sessions and the venue
 * for each session.</li>
 * <li>{@code "trainers"} - fetches the event's trainers, but not the
 * individual sessions that they are assigned to.</li>
 * <li>{@code "venues"} - fetches the event's venues, but not the individual
 * sessions held in each venue.</li>
 * <li>{@code "bookings"} - fetches the event's bookings, if the user is
 * authenticated and has the appropriate privileges for the event. This may
 * be an incomplete list, if the user does not have permission to view all
 * the event's bookings.</li>
 * </ul>
 *
 * <h4>Additional notes on the fetch parameter</h4>
 * <p>
 * In addition to the above values, the <code>fetch</code> parameter may also
 * be used in a chain using "dot" notation to fetch additional information
 * about each entity returned. For example:
 * <p>
 * <code>fetch = "themes.events"</code><br/>
 * This fetches the events's themes, and also all the events for each of
 * those themes.
 * <p>
 * <code>fetch = "programme.events.sessions"</code><br/>
 * This fetches the event's programme, all the events in that programme and
 * all the sessions in each of those events.
 * <p>
 * For more information about what may be fetched from each entity type in
 * such a chain of fetches, refer to the documentation for the relevent
 * entity type's methods: {@link ProviderMethods}, {@link ProgrammeMethods},
 * {@link ThemeMethods}, {@link CourseMethods}, {@link VenueMethods}.
 * <p>
 * The <code>fetch</code> parameter also supports fetching multiple different
 * kinds of additional information, by specifying a comma-separated list of
 * values, for example:
 * <p>
 * <code>fetch = "course.events.programme,themes.events.trainers"</code><br/>
 * This fetches all the events for the same course, and their programmes,
 * plus all the events in the same theme(s) as this event, together with
 * their trainers.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class EventMethods
{
    // The connection to the server
    private ClientConnection conn;

    /**
     * Create a new EventMethods object.
     *
     * @param conn The ClientConnection object to use to invoke methods
     * on the server.
     */
    public EventMethods(ClientConnection conn)
    {
        this.conn = conn;
    }

    /**
     * Get all events in the specified time period.
     * <p>
     * This will return any events that overlap the specified time period.
     * More specifically, it will return events whose start is less than or
     * equal to the end of the time period, and whose end is greater than or
     * equal to the start of the time period (i.e., all the start and end
     * timestamps are treated inclusively).
     * <p>
     * Optionally, this will also check the event's sessions and exclude any
     * events that have no sessions overlapping the specified time period.
     * This can happen for events with multiple sessions. For example,
     * suppose an event has 2 sessions, one on Monday and the other on
     * Friday, and that the specified time period to search was on Wednesday.
     * Then by default, the event would be returned, because it starts on
     * Monday and ends on Friday, which overlaps the time period being
     * searched, but if session checking is enabled, the event would be
     * excluded.
     * <p>
     * By default, only a few basic details about each event are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional attributes or references.
     * <p>
     * NOTE: When using this API directly via the URL endpoints, date-time
     * parameters should be supplied as either milliseconds since epoch, or
     * as ISO 8601 formatted date or date-time strings.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/event/events-in-time-period ]</code>
     *
     * @param start [optional] The start of the time period to search. If
     * omitted, this will default to 0:00am today.
     * @param end [optional] The end of the time period to search. If
     * omitted, this will default to the first midnight after the start date.
     * @param checkSessions [optional] If {@code true}, check the event
     * sessions, and exclude any events that have no sessions overlapping the
     * the time period being searched.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each event.
     *
     * @return A list of events found, in (start date-time, ID) order.
     */
    public java.util.List<UTBSEvent> getEventsInTimePeriod(java.util.Date   start,
                                                           java.util.Date   end,
                                                           boolean          checkSessions,
                                                           String           fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = {  };
        Object[] queryParams = { "start", start,
                                 "end", end,
                                 "checkSessions", checkSessions,
                                 "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/event/events-in-time-period",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.events;
    }

    /**
     * Get the event with the specified ID.
     * <p>
     * By default, only a few basic details about the event are returned, but
     * the optional <code>fetch</code> parameter may be used to fetch
     * additional details.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/event/{id} ]</code>
     *
     * @param id [required] The ID of the event to fetch.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch.
     *
     * @return The requested event or null if it was not found.
     */
    public UTBSEvent getEvent(Long      id,
                              String    fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/event/%1$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.event;
    }

    /**
     * Get the booking (if there is one) for the specified participant on this
     * event.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p> 
     * NOTE: This will return cancelled bookings, waiting list places, etc.
     * Check the booking's {@link UTBSEventBooking#status status} field to see
     * if it is a confirmed bookings.
     * <p>
     * NOTE: It is normally only possible for a participant to have one
     * booking on an event.  An exception to this is a booking with a status
     * of 'interested', which represents an expression of interest in the
     * course as a whole, rather than this specific event.  If a participant
     * has a regular booking and an 'interested' booking, this method will
     * return the regular one only.  If they have only an 'interested'
     * booking, then it will be returned.  So always check the booking's
     * {@link UTBSEventBooking#status status} field.
     * <p>
     * Note that viewing an event's bookings requires authentication as one
     * of the following:
     * <ul>
     * <li>One of the event's trainers.</li>
     * <li>The event's administrator, owner or booker.</li>
     * <li>A person with the "view-event-bookings" privilege for the event's
     * provider.</li>
     * <li>A person with the "view-3rd-party-booking" privilege for the
     * event's provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/event/{id}/bookingForCrsid/{crsid} ]</code>
     *
     * @param id [required] The ID of the event.
     * @param crsid [required] The CRSid of the participant.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The participant's booking on this event, if they have one.
     */
    public UTBSEventBooking getBookingForCrsid(Long     id,
                                               String   crsid,
                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id, crsid };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/event/%1$s/bookingForCrsid/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }

    /**
     * Get the booking (if there is one) for the specified participant on this
     * event.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p> 
     * NOTE: This will return cancelled bookings, waiting list places, etc.
     * Check the booking's {@link UTBSEventBooking#status status} field to see
     * if it is a confirmed bookings.
     * <p>
     * NOTE: It is normally only possible for a participant to have one
     * booking on an event.  An exception to this is a booking with a status
     * of 'interested', which represents an expression of interest in the
     * course as a whole, rather than this specific event.  If a participant
     * has a regular booking and an 'interested' booking, this method will
     * return the regular one only.  If they have only an 'interested'
     * booking, then it will be returned.  So always check the booking's
     * {@link UTBSEventBooking#status status} field.
     * <p>
     * Note that viewing an event's bookings requires authentication as one
     * of the following:
     * <ul>
     * <li>One of the event's trainers.</li>
     * <li>The event's administrator, owner or booker.</li>
     * <li>A person with the "view-event-bookings" privilege for the event's
     * provider.</li>
     * <li>A person with the "view-3rd-party-booking" privilege for the
     * event's provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/event/{id}/bookingForEmail/{email} ]</code>
     *
     * @param id [required] The ID of the event.
     * @param email [required] The email address of the participant.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The participant's booking on this event, if they have one.
     */
    public UTBSEventBooking getBookingForEmail(Long     id,
                                               String   email,
                                               String   fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id, email };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/event/%1$s/bookingForEmail/%2$s",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }

    /**
     * Get the bookings for the specified event.
     * <p>
     * By default, only a few basic details about each booking are returned,
     * but the optional <code>fetch</code> parameter may be used to fetch
     * additional details about each booking.
     * <p>
     * NOTE: This will return <b>all</b> bookings, including cancelled
     * bookings. Check each booking's {@link UTBSEventBooking#status status}
     * field to see which are confirmed bookings.
     * <p>
     * Note that viewing an event's bookings requires authentication as one
     * of the following:
     * <ul>
     * <li>One of the event's trainers.</li>
     * <li>The event's administrator, owner or booker.</li>
     * <li>A person with the "view-event-bookings" privilege for the event's
     * provider.</li>
     * <li>A person with the "view-3rd-party-booking" privilege for the
     * event's provider.</li>
     * </ul>
     * <p>
     * <code style="background-color: #eec;">[ HTTP: GET /api/v1/event/{id}/bookings ]</code>
     *
     * @param id [required] The ID of the event.
     * @param fetch [optional] A comma-separated list of any additional
     * details to fetch for each booking.
     *
     * @return The event's bookings, in the order they were created.
     */
    public java.util.List<UTBSEventBooking> getBookings(Long    id,
                                                        String  fetch)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id };
        Object[] queryParams = { "fetch", fetch };
        Object[] formParams = {  };
        UTBSResult result = conn.invokeMethod(Method.GET,
                                              "api/v1/event/%1$s/bookings",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.bookings;
    }

    /**
     * Update the attendance record for the specified event, recording the
     * fact that the specified person attended the specified session.
     * <p>
     * Optionally, this will create a booking record for the participant, if
     * they do not already have one. If a booking is created in this way, its
     * status will be "did not book", to indicate that the participant did
     * not book onto the event themselves.
     * <p>
     * Note that this requires authentication as a user with permission to
     * view the event's bookings and record attendance on the event.
     * Additionally, the "create-3rd-party-booking" privilege is required if
     * {@code autoCreateBooking} is set.
     * <p>
     * <code style="background-color: #eec;">[ HTTP: PUT /api/v1/event/{id}/{sessionNumber}/attendance ]</code>
     *
     * @param id [required] The ID of the event.
     * @param sessionNumber [required] The session to record attendance for.
     * @param crsid [required] The CRSid of the person who attended.
     * @param autoCreateBooking [optional] If {@code true}, automatically
     * create a booking for the person, if they do not already have one.
     * Otherwise, an error will be raised if the person has not booked.
     *
     * @return The existing or newly created booking record for the person,
     * together with all associated session bookings.
     */
    public UTBSEventBooking recordAttendance(Long       id,
                                             int        sessionNumber,
                                             String     crsid,
                                             boolean    autoCreateBooking)
        throws UTBSException, IOException, JAXBException
    {
        String[] pathParams = { ""+id, ""+sessionNumber };
        Object[] queryParams = {  };
        Object[] formParams = { "crsid", crsid,
                                "autoCreateBooking", autoCreateBooking };
        UTBSResult result = conn.invokeMethod(Method.PUT,
                                              "api/v1/event/%1$s/%2$s/attendance",
                                              pathParams,
                                              queryParams,
                                              formParams);
        if (result.error != null)
            throw new UTBSException(result.error);
        return result.booking;
    }
}
