/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import uk.ac.cam.ucs.utbs.dto.UTBSError;
import uk.ac.cam.ucs.utbs.dto.UTBSResult;

/**
 * Default implementation of the ClientConnection interface, to allow methods
 * in the University Training Booking System web service API to be invoked.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSClientConnection implements ClientConnection
{
    /** The base URL to the UTBS web service API. */
    protected String urlBase;

    /** Username for HTTP basic authentication. */
    private String username;

    /** Password for HTTP basic authentication. */
    private String password;

    /** The HTTP basic authentication authorization string. */
    private String authorization;

    /** An optional SSLSocketFactory to use on HTTPS connections. */
    protected SSLSocketFactory sf;

    /** An optional HostnameVerifier to use on HTTPS connections. */
    protected HostnameVerifier hv;

    /** Whether to ask for flattened XML (recommended for efficiency). */
    protected boolean flatXML = true;

    /** JAXB context for unmarshalling results from the server. */
    protected JAXBContext jaxbCtx;

    /**
     * Create a ClientConnection to the UTBS web service API at
     * {@code https://www.training.cam.ac.uk/}.
     * <p>
     * The connection is initially anonymous (not authenticated), but this
     * may be changed using {@link #setUsername(String)} and
     * {@link #setPassword(String)}.
     *
     * @return the connection to the UTBS server.
     */
    public static UTBSClientConnection createConnection()
        throws GeneralSecurityException, IOException, JAXBException
    {
        return new UTBSClientConnection("https://www.training.cam.ac.uk/", true);
    }

    /**
     * Create a ClientConnection to the UTBS test web service API at
     * {@code https://utbs-test.csx.cam.ac.uk/}.
     * <p>
     * The connection is initially anonymous (not authenticated), but this
     * may be changed using {@link #setUsername(String)} and
     * {@link #setPassword(String)}.
     * <p>
     * NOTE: This test server is not guaranteed to always be available, and
     * the data in it differs considerably from the data on the live system.
     *
     * @return the connection to the UTBS test server.
     */
    public static UTBSClientConnection createTestConnection()
        throws GeneralSecurityException, IOException, JAXBException
    {
        return new UTBSClientConnection("https://utbs-test.csx.cam.ac.uk/", true);
    }

    /**
     * Create a ClientConnection to a UTBS web service API running locally on
     * {@code https://localhost:8443/course-booking/}.
     * <p>
     * The connection is initially anonymous (not authenticated), but this
     * may be changed using {@link #setUsername(String)} and
     * {@link #setPassword(String)}.
     * <p>
     * This is intended for testing during development. The local server is
     * assumed to be using self-signed certificates, which will not be
     * checked.
     *
     * @return the connection to a local UTBS server.
     */
    public static UTBSClientConnection createLocalConnection()
        throws GeneralSecurityException, IOException, JAXBException
    {
        return new UTBSClientConnection("https://localhost:8443/course-booking/", false);
    }

    /**
     * Create a new ClientConnection using the specified URL base, which
     * should be something like {@code "https://www.training.cam.ac.uk/"}.
     * It is strongly recommended that certificate checking be enabled.
     * <p>
     * The connection is initially anonymous (not authenticated), but this
     * may be changed using {@link #setUsername(String)} and
     * {@link #setPassword(String)}.
     *
     * @param urlBase The base URL to the UTBS web service API.
     * @param checkCertificates If this is {@code true} the server's
     * certificates will be checked. Otherwise, the they will not, and the
     * connection may be insecure.
     * @see #createConnection()
     * @see #createTestConnection()
     */
    public UTBSClientConnection(String  urlBase,
                                boolean checkCertificates)
        throws GeneralSecurityException, IOException, JAXBException
    {
        this.urlBase = urlBase;

        // If certificate checking is disabled, create a custom socket
        // factory using a custom trust manager that trusts everything, and
        // a custom hostname verifier that allows all hosts, ignoring the
        // hosts mentioned in the certificate
        if (!checkCertificates)
        {
            TrustManager tm = new NullTrustManager();
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[] { tm }, null);

            sf = sc.getSocketFactory();
            hv = new NullHostnameVerifier();
        }

        // Create a JAXB context for unmarshalling results from the server.
        // This is relatively slow, so we do it only once here, rather than
        // in each request. Note, however, that the Unmarshaller itself is
        // not thread safe, so we do need to create separate unmarshallers
        // for each request, but they are cheap to create.
        jaxbCtx = JAXBContext.newInstance(UTBSResult.class);
    }

    /*
     * Update the authorization string for HTTP basic authentication, in
     * response to a change in the username or password.
     */
    private void updateAuthorization()
    {
        if (username != null && !username.equals(""))
        {
            try
            {
                String credentials = username + ":" + password;
                String auth = DatatypeConverter.printBase64Binary(credentials.getBytes("UTF-8"));
                authorization = "Basic " + auth;
            }
            catch (UnsupportedEncodingException e)
            {
                // Should never happen - all sensible JVMs support UTF-8
                throw new RuntimeException(e);
            }
        }
        else authorization = null;
    }

    @Override
    public void setUsername(String  username)
    {
        this.username = username;
        updateAuthorization();
    }

    @Override
    public void setPassword(String  password)
    {
        this.password = password;
        updateAuthorization();
    }

    /*
     * Format a date-time in IS0 8601 format (as used in the XML produced by
     * the server). This format is also supported by API method parameters of
     * type Date.
     */
    private String formatDate(Date  date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return DatatypeConverter.printDateTime(cal);
    }

    /**
     * Build the full URL needed to invoke a method in the web service API.
     * <p>
     * The path may contain standard Java format specifiers, which will be
     * substituted from the path parameters (suitably URL-encoded). Thus for
     * example, given the following arguments:
     * <ul>
     *     <li>{@code path = "api/v1/person/%1$s/%2$s"}
     *     <li>{@code pathParams = {"crsid", "dar17"}}
     *     <li>{@code queryParams = {"fetch", "email,title"}}
     * </ul>
     * This method will create a URL like<br>
     * {@code https://www.training.cam.ac.uk/api/v1/method/arg1/arg2?param1=value1}.
     * <p>
     * Note that all parameter values are automatically URL-encoded.
     *
     * @param path The basic path to the method, relative to the URL base.
     * @param pathParams Any path parameters that should be inserted into
     * the path in place of any format specifiers.
     * @param queryParams Any query parameters to add as part of the URL's
     * query string. These are expected to come in pairs {name1, value1,
     * name2, value2, ...}.
     * @return The complete URL.
     */
    protected URL buildURL(String       path,
                           String[]     pathParams,
                           Object...    queryParams)
        throws IOException
    {
        StringBuilder sb = new StringBuilder(urlBase);
        boolean haveQueryParams = false;
        boolean haveFlattenParam = false;

        // Substitute any path parameters
        path = path == null ? "" : path;
        if (pathParams != null)
        {
            Object[] encodedPathParams = new Object[pathParams.length];
            for (int i=0; i<pathParams.length; i++)
                if (pathParams[i] != null)
                    encodedPathParams[i] = URLEncoder.encode(pathParams[i], "UTF-8");
            path = String.format(path, encodedPathParams);
        }

        // Add the path to the common URL base
        if (sb.charAt(sb.length()-1) != '/')
            sb.append('/');

        while (path.startsWith("/"))
            path = path.substring(1);
        while (path.endsWith("/"))
            path = path.substring(0, path.length()-1);
        if (!path.isEmpty())
            sb.append(path);

        // Add any query parameters
        if (queryParams != null)
        {
            for (int i=0; i<queryParams.length-1; i+=2)
            {
                if (queryParams[i] != null && queryParams[i+1] != null)
                {
                    String name = queryParams[i].toString();
                    String value = queryParams[i+1] instanceof Date ?
                                   formatDate((Date )queryParams[i+1]) :
                                   queryParams[i+1].toString();

                    sb.append(haveQueryParams ? '&' : '?');
                    sb.append(URLEncoder.encode(name, "UTF-8"));
                    sb.append('=');
                    sb.append(URLEncoder.encode(value, "UTF-8"));
                    haveQueryParams = true;
                    if (name.equals("flatten"))
                        haveFlattenParam = true;
                }
            }
        }

        // If the flattened XML representation is being used, add the
        // "flatten" parameter, unless it has already been specified
        if (flatXML && !haveFlattenParam)
        {
            sb.append(haveQueryParams ? '&' : '?');
            sb.append("flatten=true");
        }

        // Finally create and return the full URL
        URL url = new URL(sb.toString());
        if (!"https".equalsIgnoreCase(url.getProtocol()))
            throw new ProtocolException("Illegal URL protocol - must use HTTPS");

        return url;
    }

    /**
     * Send the specified form parameters to the server
     *
     * @param conn The HTTPS connection to the server.
     * @param formParams The form parameters to submit. These are expected
     * to come in pairs {name1, value1, name2, value2, ...}.
     */
    protected void sendFormData(HttpsURLConnection  conn,
                                Object[]            formParams)
        throws IOException
    {
        if (formParams != null && formParams.length > 0)
        {
            conn.setDoOutput(true);

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            boolean firstParam = true;

            for (int i=0; i<formParams.length-1; i+=2)
            {
                if (formParams[i] != null && formParams[i+1] != null)
                {
                    String name = formParams[i].toString();
                    String value = formParams[i+1] instanceof Date ?
                                   formatDate((Date )formParams[i+1]) :
                                   formParams[i+1].toString();

                    if (!firstParam)
                        out.writeBytes("&");
                    out.writeBytes(URLEncoder.encode(name, "UTF-8"));
                    out.writeBytes("=");
                    out.writeBytes(URLEncoder.encode(value, "UTF-8"));
                    firstParam = false;
                }
            }
            out.flush();
            out.close();
        }
    }

    /**
     * Parse the response from the server (assumed to be XML) and convert
     * it into an IbisResult object.
     *
     * @param conn The HTTPS connection to the server.
     * @return The unmarshalled method result.
     */
    protected UTBSResult getXmlResult(HttpsURLConnection    conn)
        throws IOException, JAXBException
    {
        if ("application/xml".equals(conn.getContentType()))
        {
            // The result is XML, which we should be able to unmarshal
            // into an IbisResult object
            Unmarshaller unmarshaller = jaxbCtx.createUnmarshaller();

            if (conn.getResponseCode() == HttpsURLConnection.HTTP_OK)
                return (UTBSResult )unmarshaller.unmarshal(conn.getInputStream());
            else
                return (UTBSResult )unmarshaller.unmarshal(conn.getErrorStream());
        }
        else
        {
            // The result is not XML (possibly an HTML or other error) so
            // just create a UTBSResult object containing a UTBSError
            UTBSResult result = new UTBSResult();
            UTBSError error = new UTBSError();
            int respCode = conn.getResponseCode();
            Reader reader;
            char[] buffer = new char[4096];
            StringWriter sw = new StringWriter();

            result.error = error;
            error.status = respCode;
            error.message = "Unexpected result from server";

            if (respCode == HttpsURLConnection.HTTP_OK)
                reader = new InputStreamReader(conn.getInputStream(), "UTF-8");
            else
                reader = new InputStreamReader(conn.getErrorStream(), "UTF-8");

            int n = 0;
            while ((n = reader.read(buffer)) != -1)
                sw.write(buffer, 0, n);

            error.details = sw.toString();

            return result;
        }
    }

    @Override
    public UTBSResult invokeMethod(String   path,
                                   String[] pathParams,
                                   Object[] queryParams)
        throws IOException, JAXBException
    {
        return invokeMethod(Method.GET, path, pathParams, queryParams, null);
    }

    @Override
    public UTBSResult invokeMethod(Method   method,
                                   String   path,
                                   String[] pathParams,
                                   Object[] queryParams,
                                   Object[] formParams)
        throws IOException, JAXBException
    {
        URL url = buildURL(path, pathParams, queryParams);
        HttpsURLConnection conn = (HttpsURLConnection )url.openConnection();

        conn.setRequestMethod(method.toString());
        conn.setRequestProperty("Accept", "application/xml");
        if (authorization != null)
            conn.setRequestProperty("Authorization", authorization);

        if (sf != null) conn.setSSLSocketFactory(sf);
        if (hv != null) conn.setHostnameVerifier(hv);

        sendFormData(conn, formParams);

        return getXmlResult(conn).unflatten();
    }
}
