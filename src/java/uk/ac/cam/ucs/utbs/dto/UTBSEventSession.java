/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing an individual event session returned by the web
 * service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSEventSession
{
    /** The session number. */
    @XmlAttribute public Integer sessionNumber;

    /** The session's start date and time. */
    @XmlElement public Date startTime;

    /** The session's end date and time. */
    @XmlElement public Date endTime;

    /**
     * The session's venue. This will only be populated if the
     * <code>fetch</code> parameter included the "venue" option.
     */
    @XmlElement public UTBSVenue venue;

    /** Whether or not this is an optional session. */
    @XmlAttribute public Boolean optional;

    /**
     * A list of the session's trainers. This will only be populated if the
     * <code>fetch</code> parameter included the "trainers" option.
     */
    @XmlElementWrapper
    @XmlElement(name = "trainer")
    public List<UTBSSessionTrainer> trainers;

    /* == Code to help clients unflatten a UTBSEventSession object == */

    /* Flag to prevent infinite recursion due to circular references. */
    private boolean unflattened;

    /** Unflatten a single UTBSCourse. */
    protected UTBSEventSession unflatten(EntityMap  em)
    {
        if (!unflattened)
        {
            unflattened = true;
            if (venue != null)
                venue = venue.unflatten(em);
        }
        return this;
    }

    /** Unflatten a list of UTBSEventSession objects (done in place). */
    protected static void unflatten(EntityMap               em,
                                    List<UTBSEventSession>  sessions)
    {
        if (sessions != null)
            for (int i=0; i<sessions.size(); i++)
                sessions.set(i, sessions.get(i).unflatten(em));
    }
}
