/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/
package uk.ac.cam.ucs.utbs.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import uk.ac.cam.ucs.utbs.dto.UTBSResult.EntityMap;

/**
 * Class representing a person returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
public class UTBSPerson
{
    /** The unique identifier of the person. */
    @XmlAttribute public String crsid;

    /** The person's name. */
    @XmlElement public String name;

    /** The person's email address. */
    @XmlElement public String email;

    /** The person's phone number. */
    @XmlElement public String phoneNumber;

    /* == Attributes/methods for the flattened XML/JSON representation == */

    /**
     * An ID that can uniquely identify this person within the returned
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    @XmlAttribute public String id;

    /**
     * A reference (by id) to a person element in the XML/JSON document.
     * This is only used in the flattened XML/JSON representation (if the
     * "flatten" parameter is specified).
     */
    @XmlAttribute public String ref;

    /** Compute an ID that can uniquely identify this entity in XML/JSON. */
    public String id() { return "person/"+crsid; }

    /** Return a new UTBSPerson that refers to this person. */
    public UTBSPerson ref() { UTBSPerson p = new UTBSPerson(); p.ref = id; return p; }

    /* == Code to help clients unflatten a UTBSPerson object == */

    /** Unflatten a single UTBSPerson. */
    protected UTBSPerson unflatten(EntityMap    em)
    {
        return ref == null ? this : em.getPerson(ref);
    }

    /** Unflatten a list of UTBSPerson objects (done in place). */
    protected static void unflatten(EntityMap           em,
                                    List<UTBSPerson>    people)
    {
        if (people != null)
            for (int i=0; i<people.size(); i++)
                people.set(i, people.get(i).unflatten(em));
    }
}
