<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing a Lookup institution returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSInstitution extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("instid", "cancelled", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("shortName", "name", "parent",
                                       "school", "cufsCode");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("children");

    /** @var string The unique identifier of the institution. */
    public $instid;

    /** @var string The institution's short name or acronym. */
    public $shortName;

    /** @var string The institution's full name. */
    public $name;

    /**
     * @var UTBSInstitution The institution's parent institution. Only the
     * {@link instid} field will be populated unless the ``fetch`` parameter
     * included the ``"parent"`` option.
     */
    public $parent;

    /**
     * @var UTBSInstitution The institution's school institution. Only the
     * {@link instid} field will be populated unless the ``fetch`` parameter
     * included the ``"school"`` option.
     */
    public $school;

    /** @var string The institution's CUFS code. */
    public $cufsCode;

    /**
     * @var boolean A flag indicating whether this institution is cancelled
     * (and hence no longer exists).
     */
    public $cancelled;

    /**
     * @var UTBSInstitution[] A list of the institution's child institutions.
     * This will only be populated if the ``fetch`` parameter includes the
     * ``"children"`` option.
     */
    public $children;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this institution within
     * the returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to an institution element in the
     * XML/JSON document. This is only used in the flattened XML/JSON
     * representation (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSInstitution from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->cancelled))
            $this->cancelled = strcasecmp($this->cancelled, "true") == 0;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Unflatten a single UTBSInstitution.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $institution = $em->getInstitution($this->ref);
            if (!$institution->unflattened)
            {
                $institution->unflattened = true;
                if (isset($institution->parent))
                    $institution->parent = $institution->parent->unflatten($em);
                if (isset($institution->school))
                    $institution->school = $institution->school->unflatten($em);
                UTBSInstitution::unflattenInstitutions($em, $institution->children);
            }
            return $institution;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSInstitution objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSInstitution[] $institutions The institutions to unflatten.
     */
    public static function unflattenInstitutions($em, &$institutions)
    {
        if (isset($institutions))
            foreach ($institutions as $idx => $institution)
                $institutions[$idx] = $institution->unflatten($em);
    }
}
