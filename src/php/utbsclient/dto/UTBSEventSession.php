<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing an individual event session returned by the web
 * service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSEventSession extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("sessionNumber", "optional");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("startTime", "endTime", "venue");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("trainers");

    /** @var int The session number. */
    public $sessionNumber;

    /** @var DateTime The session's start date and time. */
    public $startTime;

    /** @var DateTime The session's end date and time. */
    public $endTime;

    /**
     * @var UTBSVenue The session's venue. This will only be populated if the
     * ``fetch`` parameter included the ``"venue"`` option.
     */
    public $venue;

    /** @var boolean Whether or not this is an optional session. */
    public $optional;

    /**
     * @var UTBSSessionTrainer[] A list of the session's trainers. This will
     * only be populated if the ``fetch`` parameter included the
     * ``"trainers"`` option.
     */
    public $trainers;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSEventSession from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->sessionNumber))
            $this->sessionNumber = (int )$this->sessionNumber;
        if (isset($this->optional))
            $this->optional = strcasecmp($this->optional, "true") == 0;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Overridden end element callback for XML parsing.
     *
     * @param string $tagname The name of the XML element.
     * @param string $data The textual value of the XML element.
     * @return void.
     */
    public function endChildElement($tagname, $data)
    {
        parent::endChildElement($tagname, $data);
        if ($tagname === "startTime" && isset($this->startTime))
            $this->startTime = new DateTime($this->startTime);
        if ($tagname === "endTime" && isset($this->endTime))
            $this->endTime = new DateTime($this->endTime);
    }

    /**
     * @ignore
     * Unflatten a single UTBSEventSession.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (!$this->unflattened)
        {
            $this->unflattened = true;
            if (isset($this->venue))
                $this->venue = $this->venue->unflatten($em);
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSEventSession objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSEventSession[] $sessions The sessions to unflatten.
     */
    public static function unflattenSessions($em, &$sessions)
    {
        if (isset($sessions))
            foreach ($sessions as $idx => $session)
                $sessions[$idx] = $session->unflatten($em);
    }
}
