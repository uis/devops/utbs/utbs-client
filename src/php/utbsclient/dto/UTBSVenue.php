<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing a venue returned by the web service API.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSVenue extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("venueId", "id", "ref");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("shortName", "provider", "name",
                                       "description", "capacity", "location",
                                       "directions", "disabledAccess",
                                       "parkingAdvice", "mapUrl");

    /** @var int The unique identifier of the venue. */
    public $venueId;

    /**
     * @var string The venue's unique short name (as it appears on the
     * timetable).
     */
    public $shortName;

    /**
     * @var UTBSProvider The venue's provider. This will only be populated if
     * the ``fetch`` parameter included the ``"provider"`` option.
     */
    public $provider;

    /** @var string The venue's full name. */
    public $name;

    /** @var string The venue's description. */
    public $description;

    /** @var int The number of seats or workstations in the venue. */
    public $capacity;

    /** @var string The venue's location (address). */
    public $location;

    /** @var string Directions to get to the venue. */
    public $directions;

    /**
     * @var string Information about access and emergency escape for those
     * with disabilities.
     */
    public $disabledAccess;

    /** @var string Availablity of parking near the venue. */
    public $parkingAdvice;

    /** @var string The URL of a map showing the venue's location. */
    public $mapUrl;

    /**
     * @ignore
     * @var string An ID that can uniquely identify this venue within the
     * returned XML/JSON document. This is only used in the flattened
     * XML/JSON representation (if the "flatten" parameter is specified).
     */
    public $id;

    /**
     * @ignore
     * @var string A reference (by id) to a venue element in the XML/JSON
     * document. This is only used in the flattened XML/JSON representation
     * (if the "flatten" parameter is specified).
     */
    public $ref;

    /* Flag to prevent infinite recursion due to circular references. */
    private $unflattened;

    /**
     * @ignore
     * Create a UTBSVenue from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->venueId))
            $this->venueId = (int )$this->venueId;
        $this->unflattened = false;
    }

    /**
     * @ignore
     * Overridden end element callback for XML parsing.
     *
     * @param string $tagname The name of the XML element.
     * @param string $data The textual value of the XML element.
     * @return void.
     */
    public function endChildElement($tagname, $data)
    {
        parent::endChildElement($tagname, $data);
        if ($tagname === "capacity" && isset($this->capacity))
            $this->capacity = (int )$this->capacity;
    }

    /**
     * @ignore
     * Unflatten a single UTBSVenue.
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     */
    public function unflatten($em)
    {
        if (isset($this->ref))
        {
            $venue = $em->getVenue($this->ref);
            if (!$venue->unflattened)
            {
                $venue->unflattened = true;
                if (isset($venue->provider))
                    $venue->provider = $venue->provider->unflatten($em);
            }
            return $venue;
        }
        return $this;
    }

    /**
     * @ignore
     * Unflatten a list of UTBSVenue objects (done in place).
     *
     * @param UTBSResultEntityMap $em The mapping from IDs to entities.
     * @param UTBSVenue[] $venues The venues to unflatten.
     */
    public static function unflattenVenues($em, &$venues)
    {
        if (isset($venues))
            foreach ($venues as $idx => $venue)
                $venues[$idx] = $venue->unflatten($em);
    }
}
