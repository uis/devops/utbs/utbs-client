<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSCourse.php";
require_once "UTBSDto.php";
require_once "UTBSError.php";
require_once "UTBSEvent.php";
require_once "UTBSEventBooking.php";
require_once "UTBSEventExtraInfo.php";
require_once "UTBSEventSession.php";
require_once "UTBSEventTopic.php";
require_once "UTBSInstitution.php";
require_once "UTBSPerson.php";
require_once "UTBSProgramme.php";
require_once "UTBSProvider.php";
require_once "UTBSSessionBooking.php";
require_once "UTBSSessionTrainer.php";
require_once "UTBSTheme.php";
require_once "UTBSVenue.php";

/**
 * Class representing the top-level container for all XML and JSON results.
 * This may be just a simple textual value or it may contain more complex
 * entities such as providers, programmes, events, courses, themes, venues,
 * people, bookings or institutions.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSResult extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("version");

    /* Properties marked as @XmlElement in the JAXB class */
    protected static $xmlElems = array("value", "provider", "programme",
                                       "event", "course", "theme", "venue",
                                       "person", "booking", "institution",
                                       "error", "entities");

    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("providers", "programmes", "events",
                                        "courses", "themes", "venues",
                                        "people", "bookings",
                                        "institutions");

    /** @var string The web service API version number. */
    public $version;

    /**
     * @var string The value returned by methods that return a simple textual
     * value.
     */
    public $value;

    /**
     * @var UTBSProvider The provider returned by methods that return a
     * single provider.
     *
     * Note that methods that may return multiple providers will always use
     * the {@link providers} field, even if only one provider is returned.
     */
    public $provider;

    /**
     * @var UTBSProgramme The programme returned by methods that return a
     * single programme.
     *
     * Note that methods that may return multiple programmes will always use
     * the {@link programmes} field, even if only one programme is returned.
     */
    public $programme;

    /**
     * @var UTBSEvent The event returned by methods that return a single
     * event.
     *
     * Note that methods that may return multiple events will always use the
     * {link events} field, even if only one event is returned.
     */
    public $event;

    /**
     * @var UTBSCourse The course returned by methods that return a single
     * course.
     *
     * Note that methods that may return multiple courses will always use the
     * {@link courses} field, even if only one course is returned.
     */
    public $course;

    /**
     * @var UTBSTheme The theme returned by methods that return a single
     * theme.
     *
     * Note that methods that may return multiple themes will always use the
     * {@link themes} field, even if only one theme is returned.
     */
    public $theme;

    /**
     * @var UTBSVenue The venue returned by methods that return a single
     * venue.
     *
     * Note that methods that may return multiple venues will always use the
     * {@link venues} field, even if only one venue is returned.
     */
    public $venue;

    /**
     * @var UTBSPerson The person returned by methods that return a single
     * person.
     *
     * Note that methods that may return multiple people will always use the
     * {@link people} field, even if only one person is returned.
     */
    public $person;

    /**
     * @var UTBSEventBooking The booking returned by methods that return a
     * single booking.
     *
     * Note that methods that may return multiple bookings will always use
     * the {@link bookings} field, even if only one booking is returned.
     */
    public $booking;

    /**
     * @var UTBSInstitution The institution returned by methods that return
     * a single institution.
     *
     * Note that methods that may return multiple institutions will always
     * use the {@link institutions} field, even if only one institution is
     * returned.
     */
    public $institution;

    /** @var UTBSError If the method failed, details of the error. */
    public $error;

    /**
     * @var UTBSProvider[] The list of providers returned by methods that may
     * return multiple providers. This may be empty, or contain one or more
     * providers.
     */
    public $providers;

    /**
     * @var UTBSProgramme[] The list of programmes returned by methods that
     * may return multiple programmes. This may be empty, or contain one or
     * more programmes.
     */
    public $programmes;

    /**
     * @var UTBSEvent[] The list of events returned by methods that may
     * return multiple events. This may be empty, or contain one or more
     * events.
     */
    public $events;

    /**
     * @var UTBSCourse[] The list of courses returned by methods that may
     * return multiple courses. This may be empty, or contain one or more
     * courses.
     */
    public $courses;

    /**
     * @var UTBSTheme[] The list of themes returned by methods that may
     * return multiple themes. This may be empty, or contain one or more
     * themes.
     */
    public $themes;

    /**
     * @var UTBSVenue[] The list of venues returned by methods that may
     * return multiple venues. This may be empty, or contain one or more
     * venues.
     */
    public $venues;

    /**
     * @var UTBSPerson[] The list of people returned by methods that may
     * return multiple people. This may be empty, or contain one or more
     * people.
     */
    public $people;

    /**
     * @var UTBSEventBooking[] The list of bookings returned by methods that
     * may return multiple bookings. This may be empty, or contain one or
     * more bookings.
     */
    public $bookings;

    /**
     * @var UTBSInstitution[] The list of institutions returned by methods
     * that may return multiple institutions. This may be empty, or contain
     * one or more institutions.
     */
    public $institutions;

    /**
     * @ignore
     * @var UTBSResultEntities In the flattened XML/JSON representation, all
     * the unique entities returned by the method.
     *
     * NOTE: This will be ``null`` unless the "flatten" parameter is
     * ``true``.
     */
    public $entities;

    /**
     * @ignore
     * Unflatten this UTBSResult object, resolving any internal ID refs
     * to build a fully fledged object tree.
     *
     * This is necessary if the UTBSResult was constructed from XML/JSON in
     * its flattened representation (with the "flatten" parameter set to
     * ``true``).
     *
     * On entry, the UTBSResult object may have providers, programmes,
     * events, courses, themes, venues, people, bookings or institutions in
     * it with "ref" fields referring to objects held in the "entities"
     * lists. After unflattening, all such references will have been replaced
     * by actual object references, giving an object tree that can be
     * traversed normally.
     *
     * @return UTBSResult This UTBSResult object, with its internals
     * unflattened.
     */
    public function unflatten()
    {
        if (isset($this->entities))
        {
            $em = new UTBSResultEntityMap($this);

            if (isset($this->provider))
                $this->provider = $this->provider->unflatten($em);
            if (isset($this->programme))
                $this->programme = $this->programme->unflatten($em);
            if (isset($this->event))
                $this->event = $this->event->unflatten($em);
            if (isset($this->course))
                $this->course = $this->course->unflatten($em);
            if (isset($this->theme))
                $this->theme = $this->theme->unflatten($em);
            if (isset($this->venue))
                $this->venue = $this->venue->unflatten($em);
            if (isset($this->person))
                $this->person = $this->person->unflatten($em);
            if (isset($this->booking))
                $this->booking = $this->booking->unflatten($em);
            if (isset($this->institution))
                $this->institution = $this->institution->unflatten($em);

            UTBSProvider::unflattenProviders($em, $this->providers);
            UTBSProgramme::unflattenProgrammes($em, $this->programmes);
            UTBSEvent::unflattenEvents($em, $this->events);
            UTBSCourse::unflattenCourses($em, $this->courses);
            UTBSTheme::unflattenThemes($em, $this->themes);
            UTBSVenue::unflattenVenues($em, $this->venues);
            UTBSPerson::unflattenPeople($em, $this->people);
            UTBSEventBooking::unflattenBookings($em, $this->bookings);
            UTBSInstitution::unflattenInstitutions($em, $this->institutions);
        }
        return $this;
    }
}

/**
 * @ignore
 * Class to hold the full details of all the entities returned in a result
 * (a nested class in Java and Python). This is used only in the flattened
 * result representation, where each of these entities will have a unique
 * textual ID, and be referred to from the top-level objects returned (and
 * by each other).
 *
 * In the hierarchical representation, this is not used, since all entities
 * returned will be at the top-level, or directly contained in those
 * top-level entities.
 */
class UTBSResultEntities extends UTBSDto
{
    /* Properties marked as @XmlElementWrapper in the JAXB class */
    protected static $xmlArrays = array("providers", "programmes", "events",
                                        "courses", "themes", "venues",
                                        "people", "bookings",
                                        "institutions");

    /**
     * @var UTBSProvider[] A list of all the unique providers returned by the
     * method. This may include additional providers returned as a result of
     * the ``fetch`` parameter, so this list may contain more entries than
     * the corresponding field on the enclosing class.
     */
    public $providers;

    /**
     * @var UTBSProgramme[] A list of all the unique programmes returned by
     * the method. This may include additional programmes returned as a
     * result of the ``fetch`` parameter, so this list may contain more
     * entries than the corresponding field on the enclosing class.
     */
    public $programmes;

    /**
     * @var UTBSEvent[] A list of all the unique events returned by the
     * method. This may include additional events returned as a result of the
     * ``fetch`` parameter, so this list may contain more entries than the
     * corresponding field on the enclosing class.
     */
    public $events;

    /**
     * @var UTBSCourse[] A list of all the unique courses returned by the
     * method. This may include additional courses returned as a result of
     * the ``fetch`` parameter, so this list may contain more entries than
     * the corresponding field on the enclosing class.
     */
    public $courses;

    /**
     * @var UTBSTheme[] A list of all the unique themes returned by the
     * method. This may include additional themes returned as a result of the
     * ``fetch`` parameter, so this list may contain more entries than the
     * corresponding field on the enclosing class.
     */
    public $themes;

    /**
     * @var UTBSVenue[] A list of all the unique venues returned by the
     * method. This may include additional venues returned as a result of the
     * ``fetch`` parameter, so this list may contain more entries than the
     * corresponding field on the enclosing class.
     */
    public $venues;

    /**
     * @var UTBSPerson[] A list of all the unique people returned by the
     * method. This may include additional people returned as a result of the
     * ``fetch`` parameter, so this list may contain more entries than the
     * corresponding field on the enclosing class.
     */
    public $people;

    /**
     * @var UTBSEventBooking[] A list of all the unique bookings returned by
     * the method. This may include additional bookings returned as a result
     * of the ``fetch`` parameter, so this list may contain more entries than
     * the corresponding field on the enclosing class.
     */
    public $bookings;

    /**
     * @var UTBSInstitution[] A list of all the unique institutions returned
     * by the method. This may include additional institutions returned as a
     * result of the ``fetch`` parameter, so this list may contain more
     * entries than the corresponding field on the enclosing class.
     */
    public $institutions;
}

/**
 * @ignore
 * Class to assist during the unflattening process, maintaining efficient
 * maps from IDs to entities (people, institutions and groups). This is a
 * nested class of UTBSResult in Java and Python.
 */
class UTBSResultEntityMap
{
    private $providersById;
    private $programmesById;
    private $eventsById;
    private $coursesById;
    private $themesById;
    private $venuesById;
    private $peopleById;
    private $bookingsById;
    private $institutionsById;

    /** Construct an entity map from a flattened UTBSResult. */
    public function __construct($result)
    {
        $this->providersById = array();
        $this->programmesById = array();
        $this->eventsById = array();
        $this->coursesById = array();
        $this->themesById = array();
        $this->venuesById = array();
        $this->peopleById = array();
        $this->bookingsById = array();
        $this->institutionsById = array();

        if (isset($result->entities->providers))
            foreach ($result->entities->providers as $provider)
                $this->providersById[$provider->id] = $provider;
        if (isset($result->entities->programmes))
            foreach ($result->entities->programmes as $programme)
                $this->programmesById[$programme->id] = $programme;
        if (isset($result->entities->events))
            foreach ($result->entities->events as $event)
                $this->eventsById[$event->id] = $event;
        if (isset($result->entities->courses))
            foreach ($result->entities->courses as $course)
                $this->coursesById[$course->id] = $course;
        if (isset($result->entities->themes))
            foreach ($result->entities->themes as $theme)
                $this->themesById[$theme->id] = $theme;
        if (isset($result->entities->venues))
            foreach ($result->entities->venues as $venue)
                $this->venuesById[$venue->id] = $venue;
        if (isset($result->entities->people))
            foreach ($result->entities->people as $person)
                $this->peopleById[$person->id] = $person;
        if (isset($result->entities->bookings))
            foreach ($result->entities->bookings as $booking)
                $this->bookingsById[$booking->id] = $booking;
        if (isset($result->entities->institutions))
            foreach ($result->entities->institutions as $institution)
                $this->institutionsById[$institution->id] = $institution;
    }

    /** Get a provider from the entity map, given its ID */
    public function getProvider($id) { return $this->providersById[$id]; }

    /** Get a programme from the entity map, given its ID */
    public function getProgramme($id) { return $this->programmesById[$id]; }

    /** Get an event from the entity map, given its ID */
    public function getEvent($id) { return $this->eventsById[$id]; }

    /** Get a course from the entity map, given its ID */
    public function getCourse($id) { return $this->coursesById[$id]; }

    /** Get a theme from the entity map, given its ID */
    public function getTheme($id) { return $this->themesById[$id]; }

    /** Get a venue from the entity map, given its ID */
    public function getVenue($id) { return $this->venuesById[$id]; }

    /** Get a person from the entity map, given their ID */
    public function getPerson($id) { return $this->peopleById[$id]; }

    /** Get a booking from the entity map, given its ID */
    public function getBooking($id) { return $this->bookingsById[$id]; }

    /** Get an institution from the entity map, given its ID */
    public function getInstitution($id) { return $this->institutionsById[$id]; }
}

/**
 * @ignore
 * Class to hold a XML text node's value during XML parsing.
 */
class XmlTextNode
{
    public $tagname;
    public $data;
}

/**
 * Class to parse the XML from the server and produce a UTBSResult.
 */
class UTBSResultParser
{
    /** The UTBSResult produced from the XML */
    private $result;

    /** Stack of nodes during XML parsing */
    private $nodeStack;

    /** @ignore Start element callback function for XML parsing */
    public function startElement($parser, $tagname, $attrs)
    {
        $element = null;
        if (!empty($this->nodeStack))
        {
            if ($tagname === "provider")
                $element = new UTBSProvider($attrs);
            elseif ($tagname === "programme")
                $element = new UTBSProgramme($attrs);
            elseif ($tagname === "currentProgramme")
                $element = new UTBSProgramme($attrs);
            elseif ($tagname === "event")
                $element = new UTBSEvent($attrs);
            elseif ($tagname === "course")
                $element = new UTBSCourse($attrs);
            elseif ($tagname === "topic")
                $element = new UTBSEventTopic($attrs);
            elseif ($tagname === "section")
                $element = new UTBSEventExtraInfo($attrs);
            elseif ($tagname === "session")
                $element = new UTBSEventSession($attrs);
            elseif ($tagname === "trainer")
                $element = new UTBSSessionTrainer($attrs);
            elseif ($tagname === "theme")
                $element = new UTBSTheme($attrs);
            elseif ($tagname === "venue")
                $element = new UTBSVenue($attrs);
            elseif ($tagname === "person")
                $element = new UTBSPerson($attrs);
            elseif ($tagname === "participant")
                $element = new UTBSPerson($attrs);
            elseif ($tagname === "bookedBy")
                $element = new UTBSPerson($attrs);
            elseif ($tagname === "booking")
                $element = new UTBSEventBooking($attrs);
            elseif ($tagname === "sessionBooking")
                $element = new UTBSSessionBooking($attrs);
            elseif ($tagname === "institution")
                $element = new UTBSInstitution($attrs);
            elseif ($tagname === "parent")
                $element = new UTBSInstitution($attrs);
            elseif ($tagname === "school")
                $element = new UTBSInstitution($attrs);
            elseif ($tagname === "child")
                $element = new UTBSInstitution($attrs);
            elseif ($tagname === "error")
                $element = new UTBSError($attrs);
            elseif ($tagname === "entities")
                $element = new UTBSResultEntities($attrs);
            else
            {
                $parent = end($this->nodeStack);
                if (!is_array($parent))
                    // Need a reference to the parent's child array
                    $element = &$parent->startChildElement($tagname);
            }

            if (is_null($element))
            {
                $element = new XmlTextNode();
                $element->tagname = $tagname;
            }
        }
        elseif ($tagname !== "result")
            throw new Exception("Invalid root element: '" . $tagname . "'");
        else
        {
            $element = new UTBSResult($attrs);
            $this->result = $element;
        }

        // Stack the new element. If it is an array, we must stack a
        // reference to it that we can modify.
        if (is_array($element)) $this->nodeStack[] = &$element;
        else $this->nodeStack[] = $element;
    }

    /** @ignore End element callback function for XML parsing */
    public function endElement($parser, $tagname)
    {
        if (!empty($this->nodeStack))
        {
            $element = array_pop($this->nodeStack);
            if (!empty($this->nodeStack))
            {
                if (is_array(end($this->nodeStack)))
                {
                    // Add the child to the parent's child array, which
                    // means that we must use an array reference
                    $parent = &$this->nodeStack[sizeof($this->nodeStack)-1];
                    $parent[] = $element instanceof XmlTextNode ?
                                $element->data : $element;
                }
                elseif (!(end($this->nodeStack) instanceof XmlTextNode))
                {
                    $parent = end($this->nodeStack);
                    $parent->endChildElement($tagname,
                                             $element instanceof XmlTextNode ?
                                             $element->data : $element);
                }
            }
        }
        else
            throw new Exception("Unexpected closing tag: '" . $tagname . "'");
    }

    /** @ignore Character data callback function for XML parsing */
    public function charData($parser, $data)
    {
        if (!empty($this->nodeStack))
        {
            $element = end($this->nodeStack);
            if ($element instanceof UTBSEventTopic)
            {
                if (isset($element->topic)) $element->topic .= $data;
                else $element->topic = $data;
            }
            elseif ($element instanceof UTBSSessionTrainer)
            {
                if (isset($element->name)) $element->name .= $data;
                else $element->name = $data;
            }
            elseif ($element instanceof XmlTextNode)
            {
                if (isset($element->data)) $element->data .= $data;
                else $element->data = $data;
            }
        }
    }

    /**
     * Parse XML data from the specified string and return a UTBSResult.
     *
     * @param string $data The XML string returned from the server.
     *
     * @return UTBSResult The parsed results. This may contain lists or trees
     * of objects representing people, institutions and groups returned from
     * the server.
     */
    public function parseXml($data)
    {
        $parser = xml_parser_create();
        xml_set_object($parser, $this);
        xml_set_element_handler($parser, "startElement", "endElement");
        xml_set_character_data_handler($parser, "charData");
        xml_parser_set_option ($parser, XML_OPTION_CASE_FOLDING, false);

        $this->result = null;
        $this->nodeStack = array();

        xml_parse($parser, $data);
        xml_parser_free($parser);

        return $this->result->unflatten();
    }

    /**
     * Parse XML data from the specified stream and return a UTBSResult.
     *
     * @param resource $file A file pointer to a stream containing XML
     * returned from the server.
     *
     * @return UTBSResult The parsed results. This may contain lists or trees
     * of objects representing people, institutions and groups returned from
     * the server.
     */
    public function parseXmlFile($file)
    {
        $parser = xml_parser_create();
        xml_set_object($parser, $this);
        xml_set_element_handler($parser, "startElement", "endElement");
        xml_set_character_data_handler($parser, "charData");
        xml_parser_set_option ($parser, XML_OPTION_CASE_FOLDING, false);

        $this->result = null;
        $this->nodeStack = array();

        while ($data = fread($file, 4096))
            xml_parse($parser, $data, feof($file));
        xml_parser_free($parser);

        return $this->result->unflatten();
    }
}
