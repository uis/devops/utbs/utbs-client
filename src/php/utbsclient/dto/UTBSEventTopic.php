<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once "UTBSDto.php";

/**
 * Class representing an individual topic from an event's course description
 * returned by the web service API.
 *
 * The topic may refer to the event as a whole, or be associated with a
 * specific session of the event, if the {@link sessionNumber} field is set.
 *
 * @author Dean Rasheed (dev-group@ucs.cam.ac.uk)
 */
class UTBSEventTopic extends UTBSDto
{
    /* Properties marked as @XmlAttribute in the JAXB class */
    protected static $xmlAttrs = array("sessionNumber");

    /**
     * @var int The number of session that the topic is associated with. This
     * may be null for topics describing the event as a whole.
     */
    public $sessionNumber;

    /** @var string The topic's text. */
    public $topic;

    /**
     * @ignore
     * Create a UTBSEventTopic from the attributes of an XML node.
     *
     * @param array $attrs The attributes on the XML node.
     */
    public function __construct($attrs=array())
    {
        parent::__construct($attrs);
        if (isset($this->sessionNumber))
            $this->sessionNumber = (int )$this->sessionNumber;
    }
}
