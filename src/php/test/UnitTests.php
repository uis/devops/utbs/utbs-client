<?php
/*
Copyright (c) 2013, University of Cambridge Computing Service.

This file is part of the University Training Booking System client library.

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once 'PHPUnit/Autoload.php';

require_once dirname(__FILE__) . "/../utbsclient/client/UTBSClientConnection.php";
require_once dirname(__FILE__) . "/../utbsclient/client/UTBSException.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/BookingMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/CourseMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/EventMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/InstitutionMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/PersonMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/ProgrammeMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/ProviderMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/ThemeMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/UTBSMethods.php";
require_once dirname(__FILE__) . "/../utbsclient/methods/VenueMethods.php";

class UnitTests extends PHPUnit_Framework_TestCase
{
    private static $initialised = false;
    private static $conn = null;
    private static $um = null;
    private static $provM = null;
    private static $pm = null;
    private static $em = null;
    private static $cm = null;
    private static $tm = null;
    private static $vm = null;
    private static $persM = null;
    private static $bm = null;
    private static $im = null;

    public function setUp()
    {
        if (!UnitTests::$initialised)
        {
            UnitTests::$conn = UTBSClientConnection::createLocalConnection();
            UnitTests::$um = new UTBSMethods(UnitTests::$conn);
            UnitTests::$provM = new ProviderMethods(UnitTests::$conn);
            UnitTests::$pm = new ProgrammeMethods(UnitTests::$conn);
            UnitTests::$em = new EventMethods(UnitTests::$conn);
            UnitTests::$cm = new CourseMethods(UnitTests::$conn);
            UnitTests::$tm = new ThemeMethods(UnitTests::$conn);
            UnitTests::$vm = new VenueMethods(UnitTests::$conn);
            UnitTests::$persM = new PersonMethods(UnitTests::$conn);
            UnitTests::$bm = new BookingMethods(UnitTests::$conn);
            UnitTests::$im = new InstitutionMethods(UnitTests::$conn);
            UnitTests::$initialised = true;
        }
        UnitTests::$conn->setUsername(null);

        print(" " . $this->getName() . "()\n");
    }

    // --------------------------------------------------------------------
    // UTBS tests.
    // --------------------------------------------------------------------

    public function testGetVersion()
    {
        $this->assertEquals("1.1", UnitTests::$um->getVersion());
    }

    public function testGetCurrentUser()
    {
        $this->assertNull(UnitTests::$um->getCurrentUser());

        UnitTests::$conn->setUsername("dar17");
        $this->assertEquals("dar17", UnitTests::$um->getCurrentUser());

        UnitTests::$conn->setUsername(null);
        $this->assertNull(UnitTests::$um->getCurrentUser());
    }

    public function testGetPrivileges()
    {
        try
        {
            UnitTests::$um->getPrivileges(null);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $privs = UnitTests::$um->getPrivileges(null);
        $this->assertTrue(strpos($privs, "create-provider") !== false);
    }

    // --------------------------------------------------------------------
    // Provider tests.
    // --------------------------------------------------------------------

    public function testNoSuchProvider()
    {
        $prov = UnitTests::$provM->getProvider("sldkfnlks", null);
        $this->assertNull($prov);
    }

    public function testGetProvider()
    {
        $prov = UnitTests::$provM->getProvider("UIS", "themes,venues");
        $this->assertEquals("ucs", $prov->urlPrefix);
        $this->assertEquals("UIS", $prov->institution->instid);
        $this->assertNotNull($prov->themes);
        $this->assertNotNull($prov->venues);

        $foundTheme = false;
        foreach ($prov->themes as $theme)
            if (preg_match("/.*basic.*/", $theme->shortName) &&
                preg_match("/.*Basic.*Skills.*/", $theme->title)) $foundTheme = true;
        $this->assertTrue($foundTheme);

        $foundVenue = false;
        foreach ($prov->venues as $venue)
            if ($venue->name === "New Museums Site, Hopkinson Lecture Theatre") $foundVenue = true;
        $this->assertTrue($foundVenue);

        $prov = UnitTests::$provM->getProvider("UIS", "institution");
        $this->assertEquals("University Information Services", $prov->institution->name);
    }

    public function testGetAllProviders()
    {
        $provs = UnitTests::$provM->getAllProviders("current_programme");
        $this->assertNotNull($provs);
        $this->assertTrue(sizeof($provs) > 5);

        $foundUCS = false;
        foreach ($provs as $prov)
        {
            if ($prov->shortName === "UIS")
            {
                $this->assertNotNull($prov->currentProgramme);
                $foundUCS = true;
            }
        }
        $this->assertTrue($foundUCS);
    }

    public function testProviderGetProgrammes()
    {
        $prov = UnitTests::$provM->getProvider("UIS", "programmes");
        $progs = UnitTests::$provM->getProgrammes("UIS", null);

        $this->assertTrue(sizeof($progs) > 5);
        $this->assertEquals(sizeof($progs), sizeof($prov->programmes));
        for ($i=0; $i<sizeof($progs); $i++)
        {
            $p1 = $progs[$i];
            $p2 = $prov->programmes[$i];
            $this->assertEquals($p1->id, $p2->id);
            $this->assertEquals($p1->shortName, $p2->shortName);
            $this->assertEquals($p1->title, $p2->title);
            $this->assertEquals($p1->startDate, $p2->startDate);
            $this->assertEquals($p1->endDate, $p2->endDate);
        }

        try
        {
            $progs = UnitTests::$provM->getProgrammes("UCSnosuchprovider", null);
            throw new Exception("Should have failed due to non-existent provider");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified training provider could not be found.", $e->getError()->message);
        }
    }

    public function testNoSuchProvProgrammesByDate()
    {
        try
        {
            UnitTests::$provM->getProgrammesByDate("UCSnosuchprovider", null, null, null);
            throw new Exception("Should have failed due to provider not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified training provider could not be found.", $e->getError()->message);
        }
    }

    public function testProvProgrammesByDate()
    {
        $d1 = new DateTime('2013-08-30');
        $d2 = new DateTime('2013-08-31');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(0, sizeof($progs));

        $d1 = new DateTime('2013-08-31');
        $d2 = new DateTime('2013-08-31');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(0, sizeof($progs));

        $d1 = new DateTime('2013-08-31');
        $d2 = new DateTime('2013-09-01');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(1, sizeof($progs));
        $this->assertEquals(1139396, $progs[0]->programmeId);

        $d1 = new DateTime('2013-09-01');
        $d2 = new DateTime('2013-09-01');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(1, sizeof($progs));
        $this->assertEquals(1139396, $progs[0]->programmeId);

        $d1 = new DateTime('2014-08-29');
        $d2 = new DateTime('2014-08-29');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(1, sizeof($progs));
        $this->assertEquals(1139396, $progs[0]->programmeId);

        $d1 = new DateTime('2014-08-29');
        $d2 = new DateTime('2014-08-30');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(1, sizeof($progs));
        $this->assertEquals(1139396, $progs[0]->programmeId);

        $d1 = new DateTime('2014-08-30');
        $d2 = new DateTime('2014-08-30');
        $progs = UnitTests::$provM->getProgrammesByDate("ENG", $d1, $d2);
        $this->assertEquals(0, sizeof($progs));
    }

    public function testNoSuchProvEventsInTimePeriod()
    {
        try
        {
            UnitTests::$provM->getEventsInTimePeriod("UCSnosuchprovider", null, null, false, null);
            throw new Exception("Should have failed due to provider not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified training provider could not be found.", $e->getError()->message);
        }
    }

    public function testProvEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2015-03-23T09:29:58GMT');
        $dt2 = new DateTime('2015-03-23T09:29:59GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-23T09:29:59GMT');
        $dt2 = new DateTime('2015-03-23T09:30:00GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:00GMT');
        $dt2 = new DateTime('2015-03-23T17:00:01GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:01GMT');
        $dt2 = new DateTime('2015-03-23T17:00:02GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testProvEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2015-03-11T09:29:58GMT');
        $dt2 = new DateTime('2015-03-11T09:29:59GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-11T09:29:59GMT');
        $dt2 = new DateTime('2015-03-11T09:30:00GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:00GMT');
        $dt2 = new DateTime('2015-03-11T17:30:01GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:01GMT');
        $dt2 = new DateTime('2015-03-11T17:30:02GMT');
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$provM->getEventsInTimePeriod("BIOINFO", $dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    public function testProviderGetThemes()
    {
        $themes = UnitTests::$provM->getThemes("UIS");
        $this->assertTrue(sizeof($themes) > 30);
        $this->assertEquals("assist-tech", $themes[0]->shortName);
        $this->assertEquals("Assistive Technology", $themes[0]->title);

        try
        {
            $themes = UnitTests::$provM->getThemes("UCSnosuchprovider");
            throw new Exception("Should have failed due to non-existent provider");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified training provider could not be found.", $e->getError()->message);
        }
    }

    public function testProviderGetVenues()
    {
        $venues = UnitTests::$provM->getVenues("BIOINFO");
        $this->assertTrue(sizeof($venues) >= 5);
        $this->assertEquals(1122015, $venues[0]->venueId);
        $this->assertEquals("Craik-Marshall", $venues[0]->shortName);
        $this->assertEquals("Bioinformatics Training Room, Craik-Marshall Building, Downing Site", $venues[0]->name);

        try
        {
            $venues = UnitTests::$provM->getVenues("UCSnosuchprovider");
            throw new Exception("Should have failed due to non-existent provider");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified training provider could not be found.", $e->getError()->message);
        }
    }

    // --------------------------------------------------------------------
    // Programme tests.
    // --------------------------------------------------------------------

    public function testNoSuchProgramme()
    {
        $p = UnitTests::$pm->getProgramme(-123, null);
        $this->assertNull($p);
    }

    public function testGetProgramme()
    {
        $p = UnitTests::$pm->getProgramme(1866496, "provider,events");
        $this->assertEquals(1866496, $p->programmeId);
        $this->assertEquals("ucs-f2f-ay2015-2016", $p->shortName);
        $this->assertEquals("UCS IT Skills Training 2015-2016", $p->title);

        $this->assertEquals("ucs", $p->provider->urlPrefix);
        $this->assertEquals("UIS", $p->provider->institution->instid);

        $this->assertNotNull($p->events);
        $this->assertTrue(sizeof($p->events) > 50);

        $foundEvent = false;
        foreach ($p->events as $event)
            if ($event->eventId == 1536631 &&
                $event->courseType === "instructor-led" &&
                preg_match("/Web Authoring.*/", $event->title)) $foundEvent = true;
        $this->assertTrue($foundEvent);
    }

    public function testGetProgrammesByDate()
    {
        $d1 = new DateTime('2008-08-30');
        $d2 = new DateTime('2008-08-31');
        $progs = UnitTests::$pm->getProgrammesByDate($d1, $d2);
        $this->assertEquals(0, sizeof($progs));

        $d1 = new DateTime('2008-08-31');
        $d2 = new DateTime('2008-08-31');
        $progs = UnitTests::$pm->getProgrammesByDate($d1, $d2);
        $this->assertEquals(0, sizeof($progs));

        $d1 = new DateTime('2008-08-31');
        $d2 = new DateTime('2008-09-01');
        $progs = UnitTests::$pm->getProgrammesByDate($d1, $d2);
        $this->assertEquals(2, sizeof($progs));
        $this->assertEquals(1867631, $progs[0]->programmeId);
        $this->assertEquals(2384821, $progs[1]->programmeId);

        $d1 = new DateTime('2008-09-01');
        $d2 = new DateTime('2008-09-01');
        $progs = UnitTests::$pm->getProgrammesByDate($d1, $d2);
        $this->assertEquals(2, sizeof($progs));
        $this->assertEquals(1867631, $progs[0]->programmeId);
        $this->assertEquals(2384821, $progs[1]->programmeId);

        $d1 = new DateTime('2011-11-11');
        $d2 = new DateTime('2011-11-11');
        $progs = UnitTests::$pm->getProgrammesByDate($d1, $d2);
        $this->assertEquals(27, sizeof($progs));
        $this->assertEquals(183144, $progs[0]->programmeId);
        $this->assertEquals(227599, $progs[1]->programmeId);
        $this->assertEquals(228037, $progs[2]->programmeId);
    }

    public function testNoSuchProgEventsInTimePeriod()
    {
        try
        {
            UnitTests::$pm->getEventsInTimePeriod(-123, null, null, false, null);
            throw new Exception("Should have failed due to programme not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified programme could not be found.", $e->getError()->message);
        }
    }

    public function testProgEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2015-03-23T09:29:58GMT');
        $dt2 = new DateTime('2015-03-23T09:29:59GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-23T09:29:59GMT');
        $dt2 = new DateTime('2015-03-23T09:30:00GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:00GMT');
        $dt2 = new DateTime('2015-03-23T17:00:01GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:01GMT');
        $dt2 = new DateTime('2015-03-23T17:00:02GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testProgEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2015-03-11T09:29:58GMT');
        $dt2 = new DateTime('2015-03-11T09:29:59GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-11T09:29:59GMT');
        $dt2 = new DateTime('2015-03-11T09:30:00GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:00GMT');
        $dt2 = new DateTime('2015-03-11T17:30:01GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:01GMT');
        $dt2 = new DateTime('2015-03-11T17:30:02GMT');
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$pm->getEventsInTimePeriod(1372103, $dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    public function testNoSuchProgSelfTaughtEvents()
    {
        try
        {
            UnitTests::$pm->getSelfTaughtEvents(-123, null);
            throw new Exception("Should have failed due to programme not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified programme could not be found.", $e->getError()->message);
        }
    }

    public function testProgSelfTaughtEvents()
    {
        $events = UnitTests::$pm->getSelfTaughtEvents(1867620, null);
        $this->assertEquals(0, sizeof($events));

        $events = UnitTests::$pm->getSelfTaughtEvents(1249349, "sessions,topics");
        $this->assertEquals(201, sizeof($events));
        $this->assertEquals(1249410, $events[0]->eventId);
        $this->assertEquals("Access 2007: Level 1 (Win) (Workbooks)", $events[0]->title);
        $this->assertEquals(0, sizeof($events[0]->sessions));
        $this->assertNull($events[0]->topics[0]->sessionNumber);
        $this->assertTrue(strpos($events[0]->topics[0]->topic, "Access 2007 Orientation") !== false);

        UnitTests::$conn->setUsername("dar17");
        $events = UnitTests::$pm->getSelfTaughtEvents(1249349, null);
        $this->assertEquals(209, sizeof($events));
    }

    // --------------------------------------------------------------------
    // Event tests.
    // --------------------------------------------------------------------

    public function testNoSuchEvent()
    {
        $e = UnitTests::$em->getEvent(-123, null);
        $this->assertNull($e);
    }

    public function testGetEvent()
    {
        $e = UnitTests::$em->getEvent(1536088, "provider,programme,sessions.venue,sessions.trainers");
        $this->assertEquals(1536088, $e->eventId);
        $this->assertEquals("Excel 2010/2013: Introduction", $e->title);
        $this->assertEquals("instructor-led", $e->courseType);

        $this->assertEquals("UIS", $e->provider->shortName);
        $this->assertEquals("University Information Services", $e->provider->name);

        $this->assertEquals(1866496, $e->programme->programmeId);
        $this->assertEquals("ucs-f2f-ay2015-2016", $e->programme->shortName);
        $this->assertEquals("UCS IT Skills Training 2015-2016", $e->programme->title);

        $this->assertNotNull($e->sessions);
        $this->assertEquals(2, sizeof($e->sessions));

        $this->assertEquals("UISRNBELY1", $e->sessions[0]->venue->shortName);
        $this->assertEquals("UISRNBELY2", $e->sessions[1]->venue->shortName);

        $this->assertEquals(2, sizeof($e->sessions[0]->trainers));
        $this->assertEquals(2, sizeof($e->sessions[1]->trainers));

        $this->assertEquals("ltf23", $e->sessions[0]->trainers[0]->crsid);
        $this->assertEquals("ph441", $e->sessions[0]->trainers[1]->crsid);
        $this->assertEquals("Lynn Foot", $e->sessions[1]->trainers[0]->name);
        $this->assertEquals("Paul Hunt-Deol", $e->sessions[1]->trainers[1]->name);

        $e = UnitTests::$em->getEvent(1536088, "course.events,related_courses.events");
        $this->assertTrue(sizeof($e->course->events) > 26);
        $this->assertEquals(124, $e->course->events[0]->eventId);
        $this->assertTrue(sizeof($e->relatedCourses) > 10);
        $this->assertEquals("ucs-excelbeg", $e->relatedCourses[0]->courseId);
        $this->assertTrue(sizeof($e->relatedCourses[0]->events) > 41);
        $this->assertEquals(56, $e->relatedCourses[0]->events[0]->eventId);

        $e = UnitTests::$em->getEvent(1536088, "themes,venues");
        $this->assertEquals(1, sizeof($e->themes));
        $this->assertEquals(2, sizeof($e->venues));
        $this->assertEquals("Spreadsheets", $e->themes[0]->title);
        $this->assertEquals("UISRNBELY1", $e->venues[0]->shortName);
        $this->assertEquals("UISRNBELY2", $e->venues[1]->shortName);
    }

    public function testGetEventTopics()
    {
        $e = UnitTests::$em->getEvent(1536088, "topics");
        $this->assertEquals(1536088, $e->eventId);
        $this->assertEquals(1, sizeof($e->topics));

        $t = $e->topics[0];
        $this->assertTrue(strpos($t->topic, "Building a new workbook") !== false);
    }

    public function testGetEventExtraInfo()
    {
        $e = UnitTests::$em->getEvent(1536088, "extra_info");
        $this->assertEquals(1536088, $e->eventId);
        $this->assertEquals(1, sizeof($e->extraInfo));

        $ei = $e->extraInfo[0];
        $this->assertEquals("Taught using", $ei->heading);
        $this->assertTrue(strpos($ei->content, "Excel 2010") !== false);
    }

    public function testGetEventTrainers()
    {
        $e = UnitTests::$em->getEvent(1536088, "trainers");
        $this->assertEquals(1536088, $e->eventId);
        $this->assertEquals(2, sizeof($e->trainers));

        $t1 = $e->trainers[0];
        $this->assertEquals("ltf23", $t1->crsid);
        $this->assertEquals("Lynn Foot", $t1->name);

        $t2 = $e->trainers[1];
        $this->assertEquals("ph441", $t2->crsid);
        $this->assertEquals("Paul Hunt-Deol", $t2->name);
    }

    public function testGetEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2009-05-15T09:29:58BST');
        $dt2 = new DateTime('2009-05-15T09:29:59BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2009-05-15T09:29:59BST');
        $dt2 = new DateTime('2009-05-15T09:30:00BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(2146, $events[0]->eventId);

        $dt1 = new DateTime('2009-05-15T13:00:00BST');
        $dt2 = new DateTime('2009-05-15T13:00:01BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(2146, $events[0]->eventId);

        $dt1 = new DateTime('2009-05-15T13:00:01BST');
        $dt2 = new DateTime('2009-05-15T13:00:02BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testGetEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2009-05-18T09:29:58BST');
        $dt2 = new DateTime('2009-05-18T09:29:59BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2009-05-18T09:29:59BST');
        $dt2 = new DateTime('2009-05-18T09:30:00BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(2156, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2009-05-18T12:30:00BST');
        $dt2 = new DateTime('2009-05-18T12:30:01BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(2156, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2009-05-18T12:30:01BST');
        $dt2 = new DateTime('2009-05-18T12:30:02BST');
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$em->getEventsInTimePeriod($dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(2156, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    public function testNoSuchEventBookings()
    {
        try
        {
            UnitTests::$em->getBookings(-123, null);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$em->getBookings(-123, null);
            throw new Exception("Should have failed due to event not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified event could not be found.", $e->getError()->message);
        }
    }

    public function testGetEventBookings()
    {
        try
        {
            UnitTests::$em->getBookings(150453, null);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$em->getBookings(150453, null);
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to view the bookings for this event", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $bookings = UnitTests::$em->getBookings(150453, "session_bookings");
        $this->assertTrue(sizeof($bookings) >= 10);

        $booking = null;
        foreach ($bookings as $b)
            if ($b->participant->crsid === "mg356")
                $booking = $b;

        $this->assertNotNull($booking);
        $this->assertEquals(154239, $booking->bookingId);
        $this->assertEquals("mg356", $booking->bookedBy->crsid);
        $this->assertEquals(2, sizeof($booking->sessionBookings));
        $this->assertTrue($booking->sessionBookings[0]->booked);
        $this->assertTrue($booking->sessionBookings[1]->booked);
    }

    public function testEventFetchBookings()
    {
        $e = UnitTests::$em->getEvent(10427, "bookings");
        $this->assertEquals(0, sizeof($e->bookings));

        UnitTests::$conn->setUsername("aa494");
        $e = UnitTests::$em->getEvent(10427, "bookings");
        $this->assertEquals(1, sizeof($e->bookings));
        $this->assertEquals("aa494", $e->bookings[0]->participant->crsid);

        UnitTests::$conn->setUsername("dar17");
        $e = UnitTests::$em->getEvent(10427, "bookings");
        $this->assertEquals(33, sizeof($e->bookings));
        $this->assertEquals("aa494", $e->bookings[0]->participant->crsid);
    }

    public function testRecordAttendance()
    {
        try
        {
            UnitTests::$em->recordAttendance(-123, 3, "mg356", false);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$em->recordAttendance(-123, 3, "mg356", false);
            throw new Exception("Should have failed due to event not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified event could not be found.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$em->recordAttendance(150453, 3, "mg356", false);
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to record attendance for this event", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar17");
            UnitTests::$em->recordAttendance(150453, 3, "mg356", false);
            throw new Exception("Should have failed due to session not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified session could not be found.", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, 1, false);
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);

        $b = UnitTests::$em->recordAttendance(150453, 1, "mg356", false);
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);
    }

    // --------------------------------------------------------------------
    // Course tests.
    // --------------------------------------------------------------------

    public function testNoSuchCourse()
    {
        $c = UnitTests::$cm->getCourse("lsiegf3w4in", null);
        $this->assertNull($c);
    }

    public function testGetCourse()
    {
        $c = UnitTests::$cm->getCourse("ucs-progbasics", "events,themes");
        $this->assertEquals("ucs-progbasics", $c->courseId);

        $this->assertNotNull($c->events);
        $this->assertTrue(sizeof($c->events) >= 5);

        $e = $c->events[0];
        $this->assertEquals(46, $e->eventId);
        $this->assertEquals("Programming Concepts: Introduction for Absolute Beginners", $e->title);
        $this->assertEquals("instructor-led", $e->courseType);

        $this->assertNotNull($c->themes);
        $this->assertEquals(4, sizeof($c->themes));

        $foundTheme = false;
        foreach ($c->themes as $theme)
            if ($theme->shortName === "programming-script" &&
                $theme->title === "Programming and Scripting") $foundTheme = true;
        $this->assertTrue($foundTheme);
    }

    public function testNoSuchCourseEventsInTimePeriod()
    {
        try
        {
            UnitTests::$cm->getEventsInTimePeriod("lsiegf3w4in", null, null, false, null);
            throw new Exception("Should have failed due to course not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified course could not be found.", $e->getError()->message);
        }
    }

    public function testCourseEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2015-03-23T09:29:58GMT');
        $dt2 = new DateTime('2015-03-23T09:29:59GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-ensbrow", $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-23T09:29:59GMT');
        $dt2 = new DateTime('2015-03-23T09:30:00GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-ensbrow", $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:00GMT');
        $dt2 = new DateTime('2015-03-23T17:00:01GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-ensbrow", $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:01GMT');
        $dt2 = new DateTime('2015-03-23T17:00:02GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-ensbrow", $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testCourseEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2015-03-11T09:29:58GMT');
        $dt2 = new DateTime('2015-03-11T09:29:59GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-rintro", $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-11T09:29:59GMT');
        $dt2 = new DateTime('2015-03-11T09:30:00GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-rintro", $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:00GMT');
        $dt2 = new DateTime('2015-03-11T17:30:01GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-rintro", $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:01GMT');
        $dt2 = new DateTime('2015-03-11T17:30:02GMT');
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-rintro", $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$cm->getEventsInTimePeriod("bioinfo-rintro", $dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    public function testNoSuchCourseSelfTaughtEvents()
    {
        try
        {
            UnitTests::$cm->getSelfTaughtEvents("lsiegf3w4in", null);
            throw new Exception("Should have failed due to course not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified course could not be found.", $e->getError()->message);
        }
    }

    public function testCourseSelfTaughtEvents()
    {
        $events = UnitTests::$cm->getSelfTaughtEvents("fin-ol-ch");
        $this->assertEquals("Cash Handling", $events[0]->title);
    }

    // --------------------------------------------------------------------
    // Theme tests.
    // --------------------------------------------------------------------

    public function testGetAllThemes()
    {
        $themes = UnitTests::$tm->getAllThemes();
        $this->assertTrue(sizeof($themes) > 200);
        $this->assertEquals("acadpublish", $themes[0]->shortName);
        $this->assertEquals("Academic Publishing", $themes[0]->title);
    }

    public function testNoSuchTheme()
    {
        $t = UnitTests::$tm->getTheme(-123, null);
        $this->assertNull($t);
    }

    public function testGetTheme()
    {
        $t = UnitTests::$tm->getTheme(36446, "provider.institution,events");
        $this->assertEquals(36446, $t->themeId);
        $this->assertEquals("graphics-photo", $t->shortName);
        $this->assertEquals("Graphics and Photos", $t->title);

        $this->assertEquals("ucs", $t->provider->urlPrefix);
        $this->assertEquals("UIS", $t->provider->institution->instid);
        $this->assertEquals("University Information Services", $t->provider->institution->name);

        $this->assertNotNull($t->events);
        $this->assertTrue(sizeof($t->events) >= 11);

        $foundEvent = false;
        foreach ($t->events as $e)
            if ($e->title === "Photoshop: Basic Techniques" &&
                $e->courseType === "instructor-led") $foundEvent = true;
        $this->assertTrue($foundEvent);

        $t = UnitTests::$tm->getTheme(36446, "courses.events");

        $this->assertNotNull($t->courses);
        $this->assertTrue(sizeof($t->courses) >= 10);

        $course = null;
        foreach ($t->courses as $c)
            if ($c->courseId === "ucs-photoshop") $course = $c;
        $this->assertNotNull($course);

        $foundEvent = false;
        foreach ($course->events as $e)
            if ($e->title === "Photoshop: Basic Techniques" &&
                $e->courseType === "instructor-led") $foundEvent = true;
        $this->assertTrue($foundEvent);
    }

    public function testGetThemesByName()
    {
        $themes = UnitTests::$tm->getThemesByName("presentation", null, "provider");
        $this->assertEquals(4, sizeof($themes));
        $this->assertEquals(177091, $themes[0]->themeId);
        $this->assertEquals("Presentation & Communication", $themes[0]->title);
        $this->assertEquals("GSLS", $themes[0]->provider->shortName);

        $themes = UnitTests::$tm->getThemesByName("presentation", "UIS", null);
        $this->assertEquals(1, sizeof($themes));
        $this->assertEquals(36440, $themes[0]->themeId);
    }

    public function testNoSuchThemeEventsInTimePeriod()
    {
        try
        {
            UnitTests::$tm->getEventsInTimePeriod(-123, null, null, false, null);
            throw new Exception("Should have failed due to theme not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified theme could not be found.", $e->getError()->message);
        }
    }

    public function testThemeEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2015-03-23T09:29:58GMT');
        $dt2 = new DateTime('2015-03-23T09:29:59GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355174, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-23T09:29:59GMT');
        $dt2 = new DateTime('2015-03-23T09:30:00GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355174, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:00GMT');
        $dt2 = new DateTime('2015-03-23T17:00:01GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355174, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1351263, $events[0]->eventId);

        $dt1 = new DateTime('2015-03-23T17:00:01GMT');
        $dt2 = new DateTime('2015-03-23T17:00:02GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355174, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testThemeEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2015-03-11T09:29:58GMT');
        $dt2 = new DateTime('2015-03-11T09:29:59GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355176, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-03-11T09:29:59GMT');
        $dt2 = new DateTime('2015-03-11T09:30:00GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355176, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:00GMT');
        $dt2 = new DateTime('2015-03-11T17:30:01GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355176, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-03-11T17:30:01GMT');
        $dt2 = new DateTime('2015-03-11T17:30:02GMT');
        $events = UnitTests::$tm->getEventsInTimePeriod(1355176, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$tm->getEventsInTimePeriod(1355176, $dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1346856, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    public function testNoSuchThemeSelfTaughtEvents()
    {
        try
        {
            UnitTests::$tm->getSelfTaughtEvents(-123, null);
            throw new Exception("Should have failed due to theme not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified theme could not be found.", $e->getError()->message);
        }
    }

    public function testThemeSelfTaughtEvents()
    {
        $events = UnitTests::$tm->getSelfTaughtEvents(36450);
        $this->assertEquals("Introduction to Personal Computers Using Windows 8.1 and Microsoft Office 2013", $events[0]->title);
    }

    // --------------------------------------------------------------------
    // Venue tests.
    // --------------------------------------------------------------------

    public function testGetAllVenues()
    {
        $venues = UnitTests::$vm->getAllVenues();
        $this->assertTrue(sizeof($venues) > 600);
        $this->assertEquals("16MLEAST", $venues[0]->shortName);
        $this->assertEquals("16 Mill Lane, Office of Post-Doctoral Affairs, Eastwood Room", $venues[0]->name);
        $this->assertEquals("ZOOMUEH", $venues[sizeof($venues)-1]->shortName);
        $this->assertEquals("Zoology Museum, Exhibition Hall", $venues[sizeof($venues)-1]->name);
    }

    public function testNoSuchVenue()
    {
        $v = UnitTests::$vm->getVenue(-123, null);
        $this->assertNull($v);
    }

    public function testGetVenue()
    {
        $v = UnitTests::$vm->getVenue(6, "provider");
        $this->assertEquals(6, $v->venueId);
        $this->assertEquals("NMSHOPK", $v->shortName);
        $this->assertEquals("New Museums Site, Hopkinson Lecture Theatre", $v->name);
        $this->assertNull($v->capacity);

        $this->assertEquals("ucs", $v->provider->urlPrefix);
        $this->assertEquals("UIS", $v->provider->institution->instid);
    }

    public function testNoSuchVenueEventsInTimePeriod()
    {
        try
        {
            UnitTests::$vm->getEventsInTimePeriod(-123, null, null, false, null);
            throw new Exception("Should have failed due to venue not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified venue could not be found.", $e->getError()->message);
        }
    }

    public function testVenueEventsInTimePeriod1()
    {
        $dt1 = new DateTime('2015-10-12T13:59:58+0100');
        $dt2 = new DateTime('2015-10-12T13:59:59+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-10-12T13:59:59+0100');
        $dt2 = new DateTime('2015-10-12T14:00:00+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1535799, $events[0]->eventId);

        $dt1 = new DateTime('2015-10-12T16:15:00+0100');
        $dt2 = new DateTime('2015-10-12T16:15:01+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, false, null);
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1535799, $events[0]->eventId);

        $dt1 = new DateTime('2015-10-12T16:15:01+0100');
        $dt2 = new DateTime('2015-10-12T16:15:02+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, false, null);
        $this->assertEquals(0, sizeof($events));
    }

    public function testVenueEventsInTimePeriod2()
    {
        $dt1 = new DateTime('2015-10-20T13:59:58+0100');
        $dt2 = new DateTime('2015-10-20T13:59:59+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));

        $dt1 = new DateTime('2015-10-20T13:59:59+0100');
        $dt2 = new DateTime('2015-10-20T14:00:00+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1536163, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-10-20T17:00:00+0100');
        $dt2 = new DateTime('2015-10-20T17:00:01+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, true, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1536163, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));

        $dt1 = new DateTime('2015-10-20T17:00:01+0100');
        $dt2 = new DateTime('2015-10-20T17:00:02+0100');
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, true, "sessions");
        $this->assertEquals(0, sizeof($events));
        $events = UnitTests::$vm->getEventsInTimePeriod(6, $dt1, $dt2, false, "sessions");
        $this->assertEquals(1, sizeof($events));
        $this->assertEquals(1536163, $events[0]->eventId);
        $this->assertEquals(2, sizeof($events[0]->sessions));
    }

    // --------------------------------------------------------------------
    // Person tests.
    // --------------------------------------------------------------------

    public function testNoSuchPerson()
    {
        try
        {
            UnitTests::$persM->getPerson("xxx");
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $p = UnitTests::$persM->getPerson("xxx");
        $this->assertNull($p);
    }

    public function testGetPerson()
    {
        try
        {
            UnitTests::$persM->getPerson("dar17");
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$persM->getPerson("dar17");
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to view this person", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $p = UnitTests::$persM->getPerson("dar17");
        $this->assertEquals("dar17", $p->crsid);
        $this->assertEquals("Dean Rasheed", $p->name);
        $this->assertEquals("dar17@cam.ac.uk", $p->email);
    }

    public function testGetTrainingHistory()
    {
        try
        {
            UnitTests::$persM->getTrainingHistory("dar17");
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$persM->getTrainingHistory("dar17");
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to view this person's bookings", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $bookings = UnitTests::$persM->getTrainingHistory("dar17", "event");
        $this->assertTrue(sizeof($bookings) >= 5);
        $this->assertEquals(225520, $bookings[0]->bookingId);
        $this->assertEquals(214585, $bookings[0]->event->eventId);
        $this->assertEquals("UCS: A Practical Introduction to Falcon for UCS Members", $bookings[0]->event->title);

        UnitTests::$conn->setUsername("dar99");
        $bookings = UnitTests::$persM->getTrainingHistory("dar99", null);
        $this->assertEquals(0, sizeof($bookings));
    }

    // --------------------------------------------------------------------
    // Booking tests.
    // --------------------------------------------------------------------

    public function testNoSuchBooking()
    {
        try
        {
            UnitTests::$bm->getBooking(1, null);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $b = UnitTests::$bm->getBooking(1, null);
        $this->assertNull($b);
    }

    public function testGetBooking()
    {
        try
        {
            UnitTests::$bm->getBooking(1914099, null);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$bm->getBooking(1914099, null);
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to view this booking", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $b1 = UnitTests::$bm->getBooking(1914099, "participant,booked_by,institutions");
        $this->assertEquals(1914099, $b1->bookingId);
        $this->assertEquals(1871672, $b1->event->eventId);
        $this->assertNull($b1->event->title);
        $this->assertEquals("rjd4", $b1->participant->crsid);
        $this->assertEquals("Bob Dowling", $b1->participant->name);
        $this->assertEquals("mg356", $b1->bookedBy->crsid);
        $this->assertEquals("Monica Gonzalez", $b1->bookedBy->name);
        $this->assertEquals("2016-09-23T17:46:42.479000+01:00", $b1->bookingTime->format("Y-m-d\TH:i:s.uP"));
        $this->assertEquals("UIS", $b1->institution->instid);
        $this->assertNull($b1->institution->name);
        $this->assertEquals(1, sizeof($b1->institutions));
        $this->assertEquals("UIS", $b1->institutions[0]->instid);
        $this->assertEquals("University Information Services", $b1->institutions[0]->name);

        $b2 = UnitTests::$bm->getBooking(154239, "event,participant,institution,session_bookings");
        $this->assertEquals(154239, $b2->bookingId);
        $this->assertEquals(150453, $b2->event->eventId);
        $this->assertEquals("UCS: A Practical Introduction to Falcon for UCS Members", $b2->event->title);
        $this->assertEquals("mg356", $b2->participant->crsid);
        $this->assertEquals("Monica Gonzalez", $b2->participant->name);
        $this->assertEquals("mg356", $b2->bookedBy->crsid);
        $this->assertNull($b2->bookedBy->name);
        $this->assertEquals("CS", $b2->institution->instid);
        $this->assertEquals("University Computing Service", $b2->institution->name);
        $this->assertTrue($b2->attended);
        $this->assertNull($b2->passed);
        $this->assertEquals(2, sizeof($b2->sessionBookings));

        $sb1 = $b2->sessionBookings[0];
        $this->assertEquals(1, $sb1->sessionNumber);
        $this->assertTrue($sb1->booked);
        $this->assertTrue($sb1->attended);
        $this->assertNull($sb1->passed);

        $sb2 = $b2->sessionBookings[1];
        $this->assertEquals(2, $sb2->sessionNumber);
        $this->assertTrue($sb2->booked);
        $this->assertTrue($sb2->attended);
        $this->assertNull($sb2->passed);
    }

    public function testUpdateBookingAttendance()
    {
        try
        {
            UnitTests::$bm->updateAttendance(-123, null, true);
            throw new Exception("Should have failed due to not being authenticated");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(401, $e->getError()->status);
            $this->assertEquals("This method requires authentication.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$bm->updateAttendance(-123, null, true);
            throw new Exception("Should have failed due to booking not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified booking could not be found.", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar99");
            UnitTests::$bm->updateAttendance(154239, null, true);
            throw new Exception("Should have failed due to insufficient privileges");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(403, $e->getError()->status);
            $this->assertEquals("You do not have permission to record attendance for this event", $e->getError()->message);
        }

        try
        {
            UnitTests::$conn->setUsername("dar17");
            UnitTests::$bm->updateAttendance(154239, 3, true);
            throw new Exception("Should have failed due to session not existing");
        }
        catch (UTBSException $e)
        {
            $this->assertEquals(404, $e->getError()->status);
            $this->assertEquals("The specified session could not be found.", $e->getError()->message);
        }

        UnitTests::$conn->setUsername("dar17");
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, 1, false);
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, 2, null);
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, 1, true);
        $this->assertNull($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertNull($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, null, null);
        $this->assertNull($b->attended);
        $this->assertNull($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertNull($b->attended);
        $this->assertNull($b->sessionBookings[0]->attended);
        $this->assertNull($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, null, false);
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertFalse($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertFalse($b->attended);
        $this->assertFalse($b->sessionBookings[0]->attended);
        $this->assertFalse($b->sessionBookings[1]->attended);

        $b = UnitTests::$bm->updateAttendance(154239, null, true);
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);
        $b = UnitTests::$bm->getBooking(154239, "session_bookings");
        $this->assertTrue($b->attended);
        $this->assertTrue($b->sessionBookings[0]->attended);
        $this->assertTrue($b->sessionBookings[1]->attended);
    }

    // --------------------------------------------------------------------
    // Institution tests.
    // --------------------------------------------------------------------

    public function testNoSuchInstitution()
    {
        $i = UnitTests::$im->getInstitution("XXX", null);
        $this->assertNull($i);
    }

    public function testGetInstitution()
    {
        $i = UnitTests::$im->getInstitution("UIS", "parent.children");
        $this->assertEquals("UIS", $i->instid);
        $this->assertEquals("University Information Services", $i->name);
        $this->assertEquals("INSC", $i->parent->instid);
        $this->assertEquals("Institutions Not Under the Supervision of the General Board", $i->parent->name);
        $this->assertEquals("INSC", $i->school->instid);
        $this->assertNull($i->school->name);

        $foundInst = false;
        foreach ($i->parent->children as $ii)
            if ($ii->instid === "ADCTH" &&
                $ii->name === "ADC Theatre") $foundInst = true;
        $this->assertTrue($foundInst);

        $i = UnitTests::$im->getInstitution("PHY", "school");
        $this->assertEquals("PHY", $i->instid);
        $this->assertEquals("Department of Physics", $i->name);
        $this->assertEquals("IUSCPHY", $i->school->instid);
        $this->assertEquals("Institutions under the School of the Physical Sciences", $i->school->name);
    }
}
